/* eslint-disable */
const CopyWebpackPlugin = require('copy-webpack-plugin')
module.exports = {
    pages: {
        sidebar: {
            entry: 'src/sidebar.ts',
            template: 'public/index.html',
            filename: 'sidebar.html',
            title: 'Sidebar'
        },
        background: {
            entry: 'src/background.ts',
            template: 'public/index.html',
            filename: 'background.html'
        },
        content: {
            entry: 'src/content.ts',
            template: 'public/index.html',
            filename: 'content.html'
        },
		options: {
            entry: 'src/options.ts',
            template: 'public/index.html',
            filename: 'options.html'
        },
		editor: {
			entry: 'src/editor.ts',
			template: 'public/index.html',
			filename: 'editor.html'
		},
		test: {
			entry: 'src/test.ts',
			template: 'public/index.html',
			filename: 'test.html'
		}
    },
	css: {
		extract: {
			filename: '[name].css',
			chunkFilename: '[name].css'
		}
	},
    chainWebpack: config => {
        config.optimization.delete('splitChunks')
		config.optimization.minimize(false)
    },
    configureWebpack: {
        output: {
            filename: '[name].js',
            chunkFilename: '[name].js'
        },
        plugins: [
            new CopyWebpackPlugin({
				patterns: [{
						from: './src/manifest.json'
					},
					{
						from: './src/icons/icon.png',
						to: 'icons/'
					}
				]
			})
        ],
        resolve: {
            alias: {
                'balm-ui-plus': 'balm-ui/dist/balm-ui-plus.js',
                'balm-ui-css': 'balm-ui/dist/balm-ui.css'
            }
        }
    },
    devServer: {
        inline: false
    }
}
