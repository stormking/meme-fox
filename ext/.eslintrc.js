module.exports = {
    root: true,
    env: {
        node: true
    },
    'extends': [
        'plugin:vue/vue3-essential',
        'eslint:recommended',
        '@vue/typescript'
    ],
    parserOptions: {
        ecmaVersion: 2020
    },
    rules: {
        'no-console': 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'prefer-const': 'off',
        '@typescript-eslint/no-empty-function': 'off',
        'indent': ['warn', 'tab'],
        '@typescript-eslint/ban-types': 'off'
    },
    globals: {
        browser: true
    }
}