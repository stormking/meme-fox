# Meme-Fox Browser Extension development

## Project setup
```
npm i
```

### Compiles and hot-reloads for development
```
npm run watch
npm run webext
```
Should have the server component running on localhost

### Compiles and minifies for production
```
npm run build
npm run extbuild
```
Extension is placed in `web-ext-artifacts/`
