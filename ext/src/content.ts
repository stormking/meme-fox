import { getPageInfo } from './service/content/info';
import './service/content/snapshot';
import './service/content/editor';
import { events } from './service/content/hub';
import './service/content/util';
import debounce from 'debounce';

(async function init(){
	localStorage.debug = '*';
	if (typeof document['webShareLoaded'] !== 'undefined') throw new Error('already loaded');
	console.log('content script loaded 2', browser);
	Object.defineProperty(document, 'webShareLoaded', { value: true, configurable: false, enumerable: false, writable: false });
	const recordHistoryEntry = debounce(function() {
		console.log('recordHistoryEntry', lastUrlRecorded, document.location.href);
		if (lastUrlRecorded === document.location.href) return;
		lastUrlRecorded = document.location.href;
		let info = getPageInfo();
		events.request('background', 'history.add', info);
	}, 1000);
	await document['ready'];
	let lastUrlRecorded = '';
	recordHistoryEntry();
	window.addEventListener("hashchange", recordHistoryEntry, false);
	(function(history){
		const pushState = history.pushState;
		history.pushState = function(...args) {
			recordHistoryEntry();
			return pushState.apply(history, args);
		};
		const replaceState = history.replaceState;
		history.replaceState = function(...args) {
			recordHistoryEntry();
			return replaceState.apply(history, args);
		};
	})(window.history);
	events.on('webNavigation.historyChange', ({ url }) => {
		if (document.location.href === url) {
			recordHistoryEntry();
		}
	});

})();


