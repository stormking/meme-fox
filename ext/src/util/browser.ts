
export async function getTabInfo() {
	let windowInfo = await browser.windows.getCurrent({populate: true});
	let tabs = await browser.tabs.query({windowId: windowInfo.id, active: true});
	return tabs;
}