import axios, { AxiosRequestConfig, Method } from 'axios';

let baseUrl: string;
let initResolve;
let init = new Promise(res => {
	initResolve = res;
})

async function fetch(action: string, params?: object, method?: Method) {
	if (!method) method = 'GET';
	const opts: AxiosRequestConfig = {
		method,
		url: '/api/'+action,
		data: params,
		baseURL: baseUrl,
		headers: {}
	}
	let res = await axios(opts);
	let body = (res.data);
	if (!body.ok) throw new Error(body.message || 'unknown server error');
	return body.data;
}

export async function setBackendUrl(url: string): Promise<object> {
	let old = baseUrl;
	baseUrl = url;
	let res = false;
	let info;
	try {
		info = await fetch('info');
		res = !!info.memefox;
	} catch (e) {
		console.log(e);
	}
	// console.log('setBackendUrl', res);
	if (!res) baseUrl = old;
	else initResolve(info);
	return info;
}

export async function addHistory(params) {
	await init;
	await fetch('history/add', params, 'POST');
}

export async function updateBookmarked(params) {
	await init;
	await fetch('history/updatebookmarked', params, 'POST');
}

export async function searchHistory(params) {
	await init;
	let res = await fetch(`history/search`, params, 'POST');
	return res;
}
export function stats() {
	return fetch('history/stats', {}, 'POST');
}

export async function createSnapshot(params) {
	await init;
	return fetch('resource/store', params, 'POST');
}

export async function removeSnapshot(params) {
	await init;
	return fetch('resource/remove', params, 'POST');
}

export async function getSnapshotInfoFromHash(params) {
	await init;
	return fetch('resource/forhash', params, 'POST');
}

export async function getSnapshotInfoForUrl(params) {
	await init;
	return fetch('resource/forurl', params, 'POST');
}

export async function searchSnapshots(params) {
	await init;
	return fetch('resource/search', params, 'POST');
}

