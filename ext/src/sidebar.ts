import { createApp } from 'vue'
import App from './views/sidebar.vue'
// import BalmUI from 'balm-ui'; // Official Google Material Components
// import BalmUIPlus from 'balm-ui-plus'; // BalmJS Team Material Components
import 'balm-ui-css';
// import UiButton from 'balm-ui/components/button';
// import UiCollapse from 'balm-ui/components/collapse';
// import UiSwitch from 'balm-ui/components/switch';
// import UiEditor from 'balm-ui/components/editor';
// import UiTextfield from 'balm-ui/components/textfield';
import BalmUI from 'balm-ui';
import $theme from './util/theme';

localStorage.debug = '*';
createApp(App)
	.use(BalmUI, {
		$theme
	})
	// .use(UiCollapse)
	// .use(UiSwitch)
	// .use(UiEditor)
	// .use(UiTextfield)
	.mount('#app')
