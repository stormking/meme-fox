import Debug from 'debug';
type Request = {
	id: string,
	target: string,
	sender: string,
	action: string,
	params?: object
}
type Response = {
	id: string,
	target: string,
	response?: unknown,
	error?: unknown
}
type Event = {
	event: string,
	data?: unknown
}
type Message = Request & Response & Event;
type OpenRequest = {
	id: string,
	resolve: Function,
	reject: Function,
	timer: number
}

export default class EventHub {

	private isMaster: boolean;
	private ports: browser.runtime.Port[];
	private port: browser.runtime.Port|null;
	private listeners: any;
	private name: string;
	private requestListeners = new Map<string, Function>()
	private openRequests = new Map<string, OpenRequest>()
	private debug: Debug;

	constructor(name: string) {
		this.name = name;
		this.isMaster = window.location.href.includes('_generated_background_page');
		this.ports = [];
		this.port = null;
		this.listeners = [];
		this.debug = Debug(this.name);
		if (this.isMaster) {
			this.initMaster();
		} else {
			this.initSlave();
		}
		this.debug('events created');
	}

	initMaster() {
		browser.runtime.onConnect.addListener(port => {
			this.debug('new port', port);
			this.ports.push(port);
			this.debug('connected', port.name, this.ports.length);
			port.onMessage.addListener((m: any) => {
				this.debug('incoming msg', m);
				let msg = <Message>m;
				if (msg.event) {
					this.emitToAll(msg, port);
					this.emitLocal(msg.event, msg.data);
				} else if (msg.target && msg.action) {
					if (msg.target === this.name) {
						this.requestLocal(msg);
					} else {
						this.sendToTarget(msg);
					}
				} else if (msg.target) {
					if (this.handleResponse(msg)) return;
					else this.sendResponse(msg);
				}
			});
			port.onDisconnect.addListener(() => {
				this.debug('disconnected', port.name, this.ports.length);
				let idx = this.ports.findIndex(e => e === port);
				this.ports.splice(idx, 1);
			})
		});
	}

	initSlave() {
		this.port = browser.runtime.connect({
			name: this.name
		});
		this.port.onMessage.addListener((m: any) => {
			let msg = <Message>m;
			this.debug('incoming msg', m);
			if (msg.event) {
				this.emitLocal(msg.event, msg.data);
			} else if (msg.target && msg.action) {
				this.requestLocal(msg);
			} else if(msg.target) {
				this.handleResponse(msg);
			}
		});
	}

	emit(event, data) {
		if (!this.isMaster) {
			if (this.port) this.port.postMessage({
				event,
				data
			})
		} else {
			this.emitToAll({
				event,
				data
			});
		}
		this.emitLocal(event, data);
	}

	emitLocal(event, data) {
		for (let [e, l] of this.listeners) {
			if (e !== event) continue;
			l(data, event);
		}
	}

	emitToAll(message, except?) {
		for (let port of this.ports) {
			this.debug('emit remote', port);
			if (port === except) continue;
			port.postMessage(message);
		}
	}

	on(evt, listener) {
		this.listeners.push([evt, listener]);
	}

	off(evt, listener) {
		this.listeners.filter(([e, l]) => {
			return evt && evt === e && !listener || listener === l;
		})
	}

	request(target: string, action: string, params, timeout=2000): Promise<unknown> {
		const msg: Request = { sender: this.name, id: Math.random().toString(36), target, action, params };
		let open: OpenRequest = { 
			id: msg.id,
			resolve: () => {}, 
			reject: () => {}, 
			timer: 0 
		};
		open.timer = setTimeout(() => { 
			this.openRequests.delete(open.id);
			this.debug('timeout triggered', msg);
			open.reject(new Error(`timeout on ${target}:${action}`)); 
		}, timeout);
		let p = new Promise((res, rej) => { open.resolve = res; open.reject = rej; });
		if (this.isMaster) this.sendToTarget(msg);
		else this.port?.postMessage(msg);
		this.openRequests.set(msg.id, open);
		return p;
	}

	emitEvent(eventName: string, params?: object) {
		const msg: Event = {
			event: eventName,
			data: params
		};
		this.emitToAll(msg);
	}

	async requestLocal(msg: Request) {
		let handler = this.requestListeners.get(msg.action);
		if (!handler) return Promise.reject(new Error('no handler'));
		let response, error;
		try {
			response = await handler(msg.params, msg.action, msg.sender);
		} catch (e: any) {
			error = e.message;
		}
		if (error && error.includes('STOP')) return;
		this.sendResponse({ target: msg.sender, id: msg.id, response, error });
	}

	sendResponse(msg: Response) {
		if (this.isMaster) {
			this.sendToTarget(msg);
		} else {
			if (!this.port) {
				console.warn('cannot send response, port missing', msg);
				return;
			}
			this.port?.postMessage(msg);
		}
	}

	sendToTarget(msg: Response | Request) {
		let ports = this.ports.filter(e => e.name === msg.target);
		if (ports.length) {
			ports.forEach(port => port.postMessage(msg));
		} else {
			this.debug('failed sending msg', msg);
			throw new Error('port not found: '+msg.target);
		}
	}

	handleResponse(msg: Response): boolean {
		if (!this.openRequests.has(msg.id)) return false;
		let prom = this.openRequests.get(msg.id);
		this.openRequests.delete(msg.id);
		clearTimeout(prom?.timer);
		if (typeof msg.error === 'string') prom?.reject(new Error(msg.error));
		else prom?.resolve(msg.response);
		return true;
	}

	onRequest(action: string, fn: Function) {
		this.requestListeners.set(action, fn);
	}

}
