import EventHub from '../events';

export const events = new EventHub('content-script');

export function urlGuard(handler) {
	return function(payload) {
		if (!payload || !payload.url) throw new Error('request has no url parameter!');
		if (payload.url !== document.location.href) throw new Error('STOP');
		return handler(payload);
	}
}
