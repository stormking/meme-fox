import { events, urlGuard } from './hub';

const EditorKey = '_memefox_editor';

function getEditorData() {
	const editor = document[EditorKey];
	let changes = [];
	console.log('saving editor data', editor);
	if (editor) {
		changes = JSON.parse(JSON.stringify(editor.instance.changes));
		editor.stop();
		// editor.instance.$el.remove();
		delete document[EditorKey];
	}
	return {
		html: document.documentElement.innerHTML,
		changes
	}
}

events.onRequest('editor.save', urlGuard(getEditorData));
