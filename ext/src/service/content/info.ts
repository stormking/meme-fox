import { events, urlGuard } from './hub';

const skipNodeNames = new Set([
	'SCRIPT',
	'STYLE',
	'SVG',
	'HTML',
	'HEAD',
	'TITLE',
	'META',
	'AUDIO',
	'VIDEO',
	'NOSCRIPT'
]);
const headlineNodes = new Set(['H1', 'H2', 'H3', 'H4', 'H5', 'H6']);
const maxBodyLen = 1024 * 1024;

function getTextBody() {
	let strs = new Set<string>()
	document.querySelectorAll('*').forEach(node => {
		let name = node.nodeName.toUpperCase();
		if (skipNodeNames.has(name)) return;
		let n: any = node;
		let t: string = (n.text || '').trim().toLocaleLowerCase();
		if (t.length < 3) return;
		t.split(' ').forEach(e => strs.add(e));
	})
	return Array.from(strs).join(' ');
}
function getHeadlines() {
	let headlines = new Set<string>();
	document.querySelectorAll('h1,h2,h3,h4').forEach(node => {
		let n: any = node;
		let t: string = (n.textContent || '').trim().toLocaleLowerCase();
		if (t.length < 3) return;
		t.split(' ').forEach(e => headlines.add(e));
	})
	return Array.from(headlines).join(' ');
}
function getMeta() {
	let map = new Map<string,string>();
	document.querySelectorAll('meta[name]').forEach(node => {
		let name = node.attributes.getNamedItem('name')?.nodeValue
		let content = node.attributes.getNamedItem('content')?.nodeValue;
		if (name && content) {
			map.set(name, content);
		}
	})
	// console.log(map);
	return map;
}

export function getPageInfo() {
	const res: any = {};
	let meta = getMeta();
	let body = getTextBody();
	let headlines = getHeadlines();
	res.url = document.documentURI;
	res.title = document.title;
	res.description = meta.get('description') || meta.get('twitter:description')
	res.keywords = meta.get('keywords')
	res.headlines = headlines;
	res.textContent = body;
	if (res.textContent.length > maxBodyLen) res.textContent = res.textContent.substr(0, maxBodyLen);
	res.locale = document.querySelector('html')?.attributes.getNamedItem('lang')?.nodeValue || 'en'
	console.log('pageInfo', res);
	return res;
}

events.onRequest('pageInfo', urlGuard(getPageInfo));
