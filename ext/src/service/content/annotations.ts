


type Annotation = {
	path: string,
	content: string
}
type AnnoWrap = {
	annotations: Annotation[]
}

events.on('pageAnnotations', async (wrap: AnnoWrap) => {
	await document['ready'];
	for (let annotation of wrap.annotations) {
		showAnnotation(annotation);
	}
});

function getClosestElement(path: string): Element | null {
	let res = document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE);
	console.log(path, res);
	if (res instanceof Element) return res;
	// let i = 0;
	// while(i++ < 100) {
	// 	let node = res.iterateNext();
	// 	console.log(path, node);
	// 	if (!node) break;
	// 	if (node instanceof Element) return node;
	// }
	path = path.substring(0, path.lastIndexOf('/'));
	if (path.length < 2) return null;
	return getClosestElement(path);
}

function showAnnotation(a: Annotation) {
	let elem = a.path ? getClosestElement(a.path) : null;
	console.log('show anno', a, elem);
	if (!elem) return;
	let pos = elem.getBoundingClientRect();
	let child = document.createElement('div');
	child.style.position = 'absolute';
	child.style.background = 'white';
	child.style.border = '1px solid black';
	child.style.top = `${pos.top}px`;
	child.style.left = `${pos.left}px`;
	child.innerHTML = a.content;
	document.documentElement.appendChild(child);
}
