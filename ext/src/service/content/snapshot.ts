import { events, urlGuard } from './hub';

function getDataFromBlob(img) {
	const canvas = document.createElement("canvas")
	canvas.width = img.width;
	canvas.height = img.height;
	const context = canvas.getContext("2d")
	context?.drawImage(img, 0, 0, img.width, img.height) // i assume that img.src is your blob url
	return canvas.toDataURL()
}

function makeBlobIntoData() {
	let res = document.querySelectorAll('img[src^=blob]')
	let blobs: any = [];
	for (let elem of res) {
		let img : any = elem;
		let blob = img.getAttribute('src');
		let data = getDataFromBlob(img);
		// console.log('blob2data', img, data)
		// img.setAttribute('src', data);
		blobs.push({
			link: blob,
			datauri: data
		})
	}
	return blobs;
}

function getPageSnapshot() {
	const res: any = {};
	const styles = Array.from(document.styleSheets);
	let rules = styles.filter(e => !e.href).flatMap(e => Array.from(e.cssRules))
	let styleInsert = rules.map((e:any) => e.cssText).join('\n');
	let doc : HTMLHtmlElement = document.querySelector('html')!;
	// let oldHtml = doc.innerHTML;
	let blobs = makeBlobIntoData();
	res.url = document.documentURI;
	res.html = doc.innerHTML;
	res.images = Array.from(document.images).map(e => e.src).filter(e => !!e);
	res.styles = styles.map(e => e.href).filter(e => !!e);
	res.styleInsert = styleInsert;
	res.blobs = blobs;
	// res.title = document.title;
	// res.description = document.querySelector('meta[name=descrption')?.attributes.getNamedItem('content');
	console.log('sending page info', res);
	// doc.innerHTML = oldHtml;
	return res;
}

events.onRequest('snapshot', urlGuard(getPageSnapshot));
