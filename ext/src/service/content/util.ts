
function createReady(doc, win, add, remove, loaded, load) {
	Object.defineProperty(doc, 'ready', new Promise(function (resolve) {
		if (doc.readyState === 'complete') {
			resolve(null);
		} else {
			const onReady = function() {
				resolve(null);
				doc[remove](loaded, onReady, true);
				win[remove](load, onReady, true);
			}
			doc[add](loaded, onReady, true);
			win[add](load, onReady, true);
		}
	}));
}
createReady(document, window, 'addEventListener', 'removeEventListener', 'DOMContentLoaded', 'load');
