

events.onRequest('selectPagePart', selectPagePartRequest);

let selectionEnabled = false;
const selectionColor = '#ff0';
const selectMouseOver = e => {
	if (!selectionEnabled || e.target === document) return;
	e.target.style.outline = '1px solid ' + selectionColor;
};
const selectMouseOut = e => {
	if (e.target === document) return;
	e.target.style.outline = '';
};
const getSelectClick = resolve => e => {
	e.preventDefault();
	e.stopImmediatePropagation();
	e.stopPropagation();
	const path = getXpath(e.target);
	resolve(path);
	selectMouseOut(e);
};
let selectClickHandler;



function getXpath(element) {
	// if (element.id !== '')
	// 	return 'id("'+element.id+'")';
	if (element === document.body)
		return '/html/body';

	let ix = 0;
	let siblings = element.parentNode.childNodes;
	for (let i = 0; i < siblings.length; i++) {
		let sibling = siblings[i];
		if (sibling === element)
			return getXpath(element.parentNode) + '/' + element.tagName.toLowerCase() + '[' + (ix + 1) + ']';
		if (sibling.nodeType === 1 && sibling.tagName === element.tagName)
			ix += 1;
	}
}



function selectPagePartRequest(data) {
	console.debug('selectPagePartRequest', data);
	return new Promise((resolve, reject) => {
		if (data.enabled && selectionEnabled) return reject(new Error('already running a selection'));
		if (!data.enabled && !selectionEnabled) return reject(new Error('selection wasnt enabled'));
		if (!data.enabled) {
			selectionEnabled = false;
			document.removeEventListener('mouseover', selectMouseOver);
			document.removeEventListener('mouseout', selectMouseOut);
			document.removeEventListener('click', selectClickHandler);
			return resolve(null);
		} else {
			selectionEnabled = true;
			document.addEventListener('mouseover', selectMouseOver);
			document.addEventListener('mouseout', selectMouseOut);
			selectClickHandler = getSelectClick(path => {
				selectPagePartRequest({ enabled: false });
				resolve(path);
			});
			document.addEventListener('click', selectClickHandler, false);
		}
		selectionEnabled = data.enabled;
	});
}
