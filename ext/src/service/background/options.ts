import { events } from './hub';
import * as Repo from '../../repository/server';

// Repo.setBackendUrl('http://localhost:3000');
async function setUrl() {
	let res: any = await browser.storage.local.get('serverUrl');
	let url = res.serverUrl;
	// console.log('setUrl', url);
	if (!url) url = 'http://localhost:3000/';
	let serverInfo: any = await Repo.setBackendUrl(url);
	let serverStatus = serverInfo && !!serverInfo.memefox;
	browser.storage.local.set({ serverStatus, serverInfo });
	events.emit('server.updated', { serverInfo, serverStatus, url });
	return { serverStatus, serverInfo };
}
events.onRequest('options.update', setUrl);

(async function init(){
	let { serverStatus } = await setUrl();
	if (!serverStatus) {
		browser.runtime.openOptionsPage();
	}
})();

