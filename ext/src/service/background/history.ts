import { events } from './hub';
import * as Repo from '../../repository/server';

let serverUrl = 'http://localhost:3000/'
events.on('server.updated', ({ url }) => {
	serverUrl = url;
});

events.onRequest('history.add', async (params) => {
	//if local url, do not add
	if (params.url.includes(serverUrl)) return;
	let bmRes = await browser.bookmarks.search({
		url: params.url
	});
	if (bmRes && bmRes.length) params.bookmarked = true;
	await Repo.addHistory(params);
	events.emit('history.added', params);
});

events.onRequest('history.search', async (params) => {
	return Repo.searchHistory(params);
});

events.onRequest('stats.fetch', () => {
	return Repo.stats();
});

function emitUrlUpdate({ tabId, url, timeStamp }) {
	events.emitEvent('webNavigation.historyChange', {
		tabId, url, timeStamp
	});
}

browser.webNavigation.onHistoryStateUpdated.addListener(emitUrlUpdate);
browser.webNavigation.onReferenceFragmentUpdated.addListener(emitUrlUpdate);

browser.bookmarks.onCreated.addListener(async(id: string, bm: browser.bookmarks.BookmarkTreeNode) => {
	if (!bm.url) return;
	let params = {
		url: bm.url,
		bookmarked: true
	};
	await Repo.updateBookmarked(params);
});
browser.bookmarks.onRemoved.addListener(async(id: string, info) => {
	let url = info.node.url;
	if (!url) return;
	let params = {
		url: url,
		bookmarked: false
	};
	await Repo.updateBookmarked(params);
})
