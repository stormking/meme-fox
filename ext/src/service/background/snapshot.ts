import { events } from './hub';
import * as Repo from '../../repository/server';

events.onRequest('snapshot.create', async (params) => {
	return Repo.createSnapshot(params);
});

events.onRequest('snapshot.remove', async (params) => {
	return Repo.removeSnapshot(params);
});

events.onRequest('snapshots.fromhash', async (params) => {
	return Repo.getSnapshotInfoFromHash(params);
});

events.onRequest('snapshots.forurl', async (params) => {
	return Repo.getSnapshotInfoForUrl(params);
});

events.onRequest('snapshots.search', async (params) => {
	return Repo.searchSnapshots(params);
});

async function addUserHeader(req: browser.webRequest._OnBeforeSendHeadersDetails): Promise<browser.webRequest.BlockingResponse> {
	let userRes: any = await browser.storage.local.get(['userId', 'serverInfo']);
	console.log('caught listener', req);
	if (userRes.serverInfo && userRes.serverInfo.userHeader && userRes.serverInfo.userMode === 'simple') {
		if (!userRes.userId && !req.url.match(/info$/)) throw new Error('settings missing user-id');
		let headers = req.requestHeaders;
		if (!headers!.find(e => e.name === userHeader)) {
			headers?.push({ name: userHeader, value: userRes.userId})
		}
	}
	return {
		requestHeaders: req.requestHeaders
	};
}

let serverUrl = 'http://localhost:3000/'
let userHeader = 'x-user';
events.on('server.updated', ({ url, serverInfo }) => {
	serverUrl = url;
	userHeader = serverInfo.userHeader;
	setListener();
});

function setListener() {
	let url = new URL(serverUrl);
	url.pathname = url.pathname + '*';
	url.port = '';
	browser.webRequest.onBeforeSendHeaders.removeListener(addUserHeader);
	browser.webRequest.onBeforeSendHeaders.addListener(
		addUserHeader,
		{ urls: [
			url.href
		] },
		["blocking", "requestHeaders"]
	);
}

setListener();
