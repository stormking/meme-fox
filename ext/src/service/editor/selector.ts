
let selectionEnabled = false;
let selectCb = (a) => {};
let selectionColor = '#ff0';

const onMouseOver = e => {
	if (!selectionEnabled || e.target === document) return;
	e.target.style.outline = '1px solid '+selectionColor;
}
const onMouseOut = e => {
	if (e.target === document) return;
	e.target.style.outline = '';
}
const onMouseClick = e => {
	e.preventDefault();
	e.stopImmediatePropagation();
	e.stopPropagation();
	if (!selectCb || !selectionEnabled) return;
	const path = getXpath(e.target);
	selectCb(path);
	stopSelect();
	e.target.style.outline = '';
}

function getXpath(element) {
	if (element.id !== '') {
		return 'id("'+element.id+'")';
	}
	if (element === document.body) {
		return '/html/body';
	}
	let ix = 0;
	let siblings = element.parentNode.childNodes;
	for (let i = 0; i < siblings.length; i++) {
		let sibling = siblings[i];
		if (sibling === element)
			return getXpath(element.parentNode)+'/'+element.tagName.toLowerCase()+'['+(ix+1)+']';
		if (sibling.nodeType === 1 && sibling.tagName === element.tagName)
			ix += 1;
	}
}

export function startSelect(cb) {
	selectCb = cb;
	selectionEnabled = true;
	setTimeout(() => {
		document.addEventListener('mouseover', onMouseOver);
		document.addEventListener('mouseout', onMouseOut);
		document.addEventListener('click', onMouseClick, false);
	});
}

export function stopSelect() {
	selectionEnabled = false;
	selectCb = (a) => {};
	setTimeout(() => {
		document.removeEventListener('mouseover', onMouseOver);
		document.removeEventListener('mouseout', onMouseOut);
		document.removeEventListener('click', onMouseClick);
	})
}
