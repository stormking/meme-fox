
function getElemsFromXpath(xpath): HTMLElement[] {
	let res = document.evaluate(xpath, document);
	let elems: HTMLElement[] = [];
	let elem;
	while (elem = res.iterateNext()) {	//eslint-disable-line no-cond-assign
		elems.push(elem);
	}
	if (elems.length === 0) throw new Error('xpath not found: '+xpath);
	return elems;
}
type Action = {
	xpath: string,
	node: string,
	value: string | null
}
export type Change = {
	label: string,
	run: Action[],
	undo: Action[]
}

/*
	hide:
		visibility: hidden
	remove:
		display: none
	text:
		textContent = param
	annotate:
		data-attr, position rel
	highlight text
		innerHTML
*/

export function createHideElement(xpath): Change {
	const change: Change = {
		label: `Hide`,
		run: [],
		undo: []
	};
	const node = 'style:visibility';
	let [elem] = getElemsFromXpath(xpath);
	let currentValue = elem.style.visibility;
	change.run.push({
		xpath,
		node,
		value: 'hidden'
	});
	change.undo.push({
		xpath,
		node,
		value: currentValue
	});
	return change;
}

export function createRemoveElement(xpath): Change {
	const change: Change = {
		label: `Remove`,
		run: [],
		undo: []
	};
	const node = 'style:display';
	let [elem] = getElemsFromXpath(xpath);
	let currentValue = elem.style.display;
	change.run.push({
		xpath,
		node,
		value: 'none'
	});
	change.undo.push({
		xpath,
		node,
		value: currentValue
	});
	return change;
}

export function createHtmlChange(xpath, html): Change {
	const change: Change = {
		label: `content`,
		run: [],
		undo: []
	};
	const node = 'innerHTML';
	let [elem] = getElemsFromXpath(xpath);
	let currentValue = elem.innerHTML;
	change.run.push({
		xpath,
		node,
		value: html
	});
	change.undo.push({
		xpath,
		node,
		value: currentValue
	});
	return change;
}

export function createAnnotation(xpath, text): Change {
	const change: Change = {
		label: `annotation`,
		run: [],
		undo: []
	};
	let [elem] = getElemsFromXpath(xpath);
	let currentPosition = elem.style.position;
	let currentAnnotation = elem.getAttribute('data-annotation');
	change.run.push({
		xpath,
		node: 'style:position',
		value: 'relative'
	});
	change.run.push({
		xpath,
		node: 'attr:data-annotation',
		value: text
	});
	change.undo.push({
		xpath,
		node: 'style:position',
		value: currentPosition
	});
	change.undo.push({
		xpath,
		node: 'attr:data-annotation',
		value: currentAnnotation
	});
	return change;
}

function getValueFromNode(elem, node) {
	let path = node.split('.');
	let n = elem;
	for (let p of path) {
		if (!(p in n)) throw new Error('node not found: '+p);
		n = n[p];
	}
	return n;
}
function setValueInNode(elem: HTMLElement, node, value) {
	let [type, path] = node.split(':');
	if (type === 'innerHTML') {
		elem.innerHTML = value;
	} else if (type === 'style') {
		elem.style[path] = value;
	} else if (type === 'attr') {
		elem.setAttribute(path, value);
	} else {
		throw new Error('unknown node type: '+type);
	}
}

export function runChange(change: Change, undo: boolean) {
	let actions = undo ? change.undo : change.run;
	for (let a of actions) {
		let [elem] = getElemsFromXpath(a.xpath);
		setValueInNode(elem, a.node, a.value);
	}
}

export function testChange(change: Change, undo: boolean) {

}

export function getExistingAnnotation(xpath: string) {
	let [elem] = getElemsFromXpath(xpath);
	return elem.getAttribute('data-annotation');
}

// export function addChange([action,path,params]) {
// 	switch (action) {
// 	default: {
// 		throw new Error('unknown action: '+action);
// 	}
// 	case 'hide': {
// 		let elems = getElemsFromXpath(path);
// 		for (let e of elems) {
// 			if ('style' in e) {
// 				e.style.display = 'none';
// 			}
// 		}
// 	}

// 	}
	
// }

// export function undoChange([action, path, params]) {
// 	switch (action) {
// 	default: {
// 		throw new Error('unknown action: '+action);
// 	}
// 	case 'hide': {
// 		let elems = getElemsFromXpath(path);
// 		for (let e of elems) {
// 			if ('style' in e) {
// 				e.style.display = 'inherit';
// 			}
// 		}
// 	}
// 	}
// }
