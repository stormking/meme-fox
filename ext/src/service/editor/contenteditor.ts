
let editing: HTMLElement | null = null;
let oldContent = '';

export function startEditing(xpath: string) {
	stopEditing();
	let res = document.evaluate(xpath, document);
	let elem: any = res.iterateNext();
	editing = elem;
	oldContent = elem.innerHTML;
	elem.setAttribute('contenteditable', 'true');
}

export function stopEditing() {
	if (!editing) return;
	editing.removeAttribute('contenteditable');
	let content = editing.innerHTML;
	editing.innerHTML = oldContent;
	editing = null;
	return content;
}
