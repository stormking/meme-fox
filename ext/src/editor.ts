import { createApp } from 'vue'
import App from './views/editor.vue'

const EditorKey = '_memefox_editor';
const EditorInitChangesKey = '_memefox_editor_init';
console.log('editor init', document.location.href);
if (document[EditorKey]) {
	console.log('Editor already loaded');
} else {
	console.log('Editor not loaded');
	if (document.location.search.includes('resolveLocal=')) {
		console.log('is an edited file, refusing to start');
	} else {
		const container = document.createElement('div');
		document.body.appendChild(container);

		const app = createApp(App);
		const instance = app.mount(container);
		const stop = () => { app.unmount(); container.remove(); };
		document[EditorKey] = { app, instance, stop };
		if (document[EditorInitChangesKey]) {
			let changes = document[EditorInitChangesKey];
			instance['changes'] = changes;
		}
		console.log('EDITOR LOADED', app);
	}
}

