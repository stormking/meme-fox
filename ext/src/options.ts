import { createApp } from 'vue'
import App from './views/options.vue'
import 'balm-ui-css';
import UiButton from 'balm-ui/components/button';
import UiSwitch from 'balm-ui/components/switch';
import UiTextfield from 'balm-ui/components/textfield';

localStorage.debug = '*';
createApp(App)
	.use(UiSwitch)
	.use(UiTextfield)
	.use(UiButton)
	.mount('#app')
