
export type Result = {
	url: string,
	title: string,
	description: string,
	firstVisited: Date,
	lastVisited: Date,
	timesVisited: number
}
