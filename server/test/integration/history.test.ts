import { describe, it } from 'mocha';
import { expect } from 'chai';
import * as Common from '../util/common.js';

describe('History', () => {

	// let server;
	before(async() => {
		return Common.startServer();
	});

	after(async() => {
		return Common.stopServer();
	});

	const url1 = 'http://example.com/a';
	const title1 = 'title of my first website';
	const description1 = 'i am a long and boring descriptive text';
	const description1b = 'hifi equipment';
	const textContent = 'the body text about long content and many descriptive products';
	let entry1;
	it('can add a history entry', async() => {
		let res = await Common.request('history/add', {
			url: url1,
			title: title1,
			description: description1,
			textContent,
			bookmarked: true
		}, 'post');
		// console.log(res);
		expect(res.history).to.be.a('number');
		entry1 = res.history;
	});

	it('can search for the history with one word', async() => {
		let res = await Common.request('history/search', { query: 'title', locale: 'en' }, 'post');
		// console.log(res);
		expect(res.entries.length).to.equal(1);
		let e = res.entries[0];
		expect(e).to.have.property('firstVisited')
		expect(e).to.have.property('lastVisited')
		expect(e.timesVisited).to.equal(1);
		expect(e.title).to.equal(title1)
		expect(e.description).to.equal(description1)
		expect(e.url).to.equal(url1)
	});

	it('can search for the history with more words', async() => {
		let res = await Common.request('history/search', { query: 'title and other different words', locale: 'en' }, 'post');
		expect(res.entries.length).to.equal(1);
	});

	const url2 = 'http://example.com/b';
	const title2 = 'tec-topico';
	const description2 = 'another seo text';

	it('can add another history entry', async() => {
		let res = await Common.request('history/add', {
			url: url2,
			title: title2,
			description: description2,
			textContent
		}, 'post');
		// console.log(res);
		expect(res.history).to.be.a('number');
	});

	it('can find only the second entry', async() => {
		let res = await Common.request('history/search', { query: 'topico', locale: 'en' }, 'post');
		expect(res.entries.length).to.equal(1);
		expect(res.entries[0].url).to.equal(url2);
	});

	it('can find both entries', async() => {
		let res = await Common.request('history/search', { query: 'topico boring', locale: 'en' }, 'post');
		expect(res.entries.length).to.equal(2);
	});

	it('can find by url', async() => {
		let res = await Common.request('history/search', { url: 'http://example.com/b' }, 'post');
		expect(res.entries.length).to.equal(1);
	});

	it('can find by bookmarked', async() => {
		let res = await Common.request('history/search', { bookmarked: true }, 'post');
		expect(res.entries.length).to.equal(1);
	});

	it('can update the first entry title', async() => {
		let res = await Common.request('history/add', {
			url: url1,
			title: title1,
			description: description1b,
			textContent
		}, 'post');
		// console.log(res);
		expect(res.history).to.equal(entry1);
	});

	it('can no longer find it under the old text', async() => {
		let res = await Common.request('history/search', { query: 'boring', locale: 'en' }, 'post');
		expect(res.entries.length).to.equal(0);
		// Q: but should it?
	});

	it('can filter by bookmarked', async() => {
		let res = await Common.request('history/search', { query: 'hifi', locale: 'en', bookmarked: true }, 'post');
		expect(res.entries.length).to.equal(0);
	});

	it('can filter by lastVisited', async() => {
		let res = await Common.request('history/search', { query: 'hifi', locale: 'en', lastVisitedAfter: '2020-01-01 12:32:10' }, 'post');
		expect(res.entries.length).to.equal(1);
	});

	it('can retrieve stats', async() => {
		let stats = await Common.request('history/stats', null, 'post');
		// console.log(stats);
		expect(stats.metaSize).to.be.greaterThan(10000);
		expect(stats.visitedToday).to.equal(2);
		expect(stats.addedToday).to.equal(2);
	})

	it('can search without query', async() => {
		let res = await Common.request('history/search', { }, 'post');
		// console.log(res);
		expect(res.entries.length).to.equal(2);
	});

});
