import { describe, it } from 'mocha';
import { expect } from 'chai';
import * as Common from '../util/common.js';

describe('Users', () => {

	const historyAddReq = {
		url: 'http://example.org/b',
		title: 'some generic title',
		description: 'a really good descriptive text',
		textContent: 'text and other stuff'
	}
	const bookmarkAddReq = {
		url: historyAddReq.url,
		title: historyAddReq.title,
		description: historyAddReq.description
	}

	describe('one-user-mode', () => {
		before(async() => {
			process.env.USER_HEADER = '';
			return Common.startServer();
		});
		after(async() => {
			return Common.stopServer();
		});
		it('can send request without user-header', async() => {
			let res = await Common.request('history/add', historyAddReq, 'post');
			expect(res.history).to.be.a('number');
		});
	});

	describe('multi-user-mode', () => {
		before(async() => {
			process.env.USER_HEADER = 'x-user';
			process.env.USER_MODE = 'simple';
			return Common.startServer();
		});
		after(async() => {
			return Common.stopServer();
		});

		it('cannot send data without header', async() => {
			let thrown = false;
			try {
				await Common.request('history/add', historyAddReq, 'post');
			} catch (e) {
				thrown = true;
				expect(e.message).to.include('code 401');
			}
			expect(thrown).to.be.true;
		});

		it('can get server info without header', async() => {
			let res = await Common.request('info', null, 'get');
			expect(res).to.deep.equal({
				memefox: 'v0.5',
				userMode: 'simple',
				userHeader: 'x-user',
				userWhitelist: false
			});
		});

		it('will restrict history by user', async() => {
			await Common.request('history/add', historyAddReq, 'post', 'u-1');
			await Common.request('history/add', historyAddReq, 'post', 'u-2');
			await Common.request('history/add', historyAddReq, 'post', 'u-1');
			let res = await Common.request('history/search', { query: 'generic' }, 'post', 'u-1');
			expect(res.count).to.equal(1);
			expect(res.entries[0].timesVisited).to.equal(2);
		});

		it('will restrict snapshots by user', async() => {
			await Common.request('resource/store', {
				uri: 'http://localhost:9994/',
				mime: 'text/html',
				content: '<html><body>something</body></html>',
				changes: []
			}, 'POST', 'u-1');
			let res = await Common.request('resource/search', {
				url: 'http://localhost:9994/'
			}, 'POST', 'u-2');
			expect(res.count).to.equal(0);
		})
	});

});
