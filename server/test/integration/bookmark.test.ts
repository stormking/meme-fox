import { describe, it } from 'mocha';
import { expect } from 'chai';
import * as Common from '../util/common.js';

describe('Bookmark', () => {
	
	before(async() => {
		return Common.startServer();
	});

	after(async() => {
		return Common.stopServer();
	});

	const url1 = 'http://example.com/a';
	const url2 = 'http://example.com/c' + (Math.random()*1000).toString(36);
	const title1 = 'title of my first website';
	const description1 = 'i am a long and boring descriptive text';
	let entry1;
	it('can add a bookmark', async() => {
		let res = await Common.request('bookmark/set', {
			url: url1,
			title: title1,
			description: description1
		}, 'post');
		expect(res).to.be.true;
		entry1 = res.history;
	});

	it('can search for bookmarks by memo', async() => {
		let res = await Common.request('bookmark/list', {
			query: 'first',
			sortCol: 'score',
			sortDir: 'desc',
			offset: 0,
			limit: 10
		}, 'post');
		expect(res.length).to.equal(1);
		// console.log(res);
	});

	it('can search for bookmarks by page title', async() => {
		let res = await Common.request('bookmark/list', {
			query: 'title website',
			sortCol: 'score',
			sortDir: 'desc',
			offset: 0,
			limit: 10
		}, 'post');
		expect(res.length).to.equal(1);
	});

	it('can search for bookmarks by domain', async() => {
		let res = await Common.request('bookmark/list', {
			url: 'https://example.com/',
			sortCol: 'score',
			sortDir: 'desc',
			offset: 0,
			limit: 10
		}, 'post');
		expect(res.length).to.equal(1);
	});

	it('can remove a bookmark', async() => {
		let res = await Common.request('bookmark/remove', {
			url: url1
		}, 'post');
		expect(res).to.be.true;
	});

	it('can add a bookmark for a never visited url', async() => {
		let res = await Common.request('bookmark/set', {
			url: url2,
			title: title1,
			description: description1
		}, 'post');
		expect(res).to.be.true;
		entry1 = res.history;
	});

	describe('import bookmarks', () => {

		before(async() => {
			await Common.startStaticServer();
		});
		after(async() => {
			await Common.stopStaticServer();
		})

		it('can start bookmark import', async() => {
			let res = await Common.request('bookmark/import', {
				list: [{
					url: 'http://localhost:9994/',
					title: 'my bookmark title'
				}]
			}, 'post');
			expect(res.queued).to.equal(1);
		});

		it('can query until queue is done', async() => {
			let n = 0;
			let res;
			while(++n < 20) {
				res = await Common.request('bookmark/import', {
					list: []
				}, 'post');
				if (res.queued === 0) break;
				await Common.sleep(100);
			}
			expect(res.queued).to.equal(0);
		});

		it('can find bookmark', async() => {
			let info = await Common.request('bookmark/get', { url: 'http://localhost:9994/' }, 'post');
			expect(info).to.be.an('object');
			expect(info.title).to.equal('my bookmark title');

		});

	})

});
