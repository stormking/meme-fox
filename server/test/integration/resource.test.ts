import { describe, it } from 'mocha';
import { expect } from 'chai';
import * as Common from '../util/common.js';
import connection from '../../src/db/base.js';

describe('Resource', () => {
	
	before(async() => {
		await Common.startServer();
		await Common.startStaticServer();
	});
	after(async() => {
		await Common.stopServer();
		await Common.stopStaticServer();
	});

	let res1, res2;
	const textContent = 'i am a sample text blob';
	it('can store a resource', async() => {
		res1 = await Common.request('resource/store', {
			uri: 'http://example.org/res1',
			mime: 'text/plain',
			content: textContent
		}, 'POST');
		// console.log(res1);
		expect(res1.uid).to.be.a('number');
		expect(res1.hash).to.be.a('string');
		expect(res1.created).to.be.a('string');

	});

	it('can fetch a resource', async() => {
		let res = await Common.fetchStatic('res/'+res1.hash);
		expect(res).to.equal(textContent);
	});

	it('can store a webpage with references', async() => {
		let html = await Common.fetchStatic('index.html', 9994);
		res1 = await Common.request('resource/store', {
			uri: 'http://localhost:9994/index.html',
			mime: 'text/html',
			content: html
		}, 'POST');
		let res = await connection('resource_content').count('hash');
		expect(res[0].count).to.equal('5', 'should have stored 5 resources total');
	});

	it('can fetch the changed webpage', async() => {
		let res = await Common.fetchStatic('res/'+res1.hash);
		let m = res.match(/href="[0-9a-h]{40}\.css"/);
		expect(m).to.not.equal(null);
	});

	it('can store a second version of the webpage', async() => {
		let html = await Common.fetchStatic('res/'+res1.hash);
		html = html.replace('index 1', 'index 2');
		res1 = await Common.request('resource/store', {
			uri: 'http://localhost:9994/index.html',
			mime: 'text/html',
			content: html,
			isSnapshot: true,
			changes: [{
				label: 'content',
				run: [{
					xpath: '/body/h1',
					node: 'innerHTML',
					value: 'text index 2'
				}],
				undo: [{
					xpath: '/body/h1',
					node: 'innerHTML',
					value: 'text index 1'
				}]
			}]
		}, 'POST');
		// console.log(res1);
		let res = await connection('resource_content').count('hash');
		expect(res[0].count).to.equal('6', 'should have stored 6 resources total');
	});

	let urlinfo;
	it('can fetch infos about an url', async() => {
		urlinfo = await Common.request('resource/forurl', {
			uri: 'http://localhost:9994/index.html'
		}, 'POST');
		expect(urlinfo.length).to.equal(2);
		expect(urlinfo[0].hash).to.not.equal(urlinfo[1].hash);
		// console.log(urlinfo);
	});

	it('can store a page with blobs', async() => {
		let html = await Common.fetchStatic('index.html', 9994);
		html = html.replace('nothing', 'blob:localhost/test');
		let img = await Common.fetchStatic('css/bg1.jpg', 9994, true);
		res2 = await Common.request('resource/store', {
			uri: 'http://localhost:9994/index.html',
			mime: 'text/html',
			content: html,
			styleInsert: 'img { padding: 2em; border: 3px solid #000; }',
			blobs: [{
				link: 'blob:localhost/test',
				datauri: 'data:image/jpeg;base64,'+Buffer.from(img).toString('base64')
			}],
			changes: []
		}, 'POST');
		let res = await connection('resource_content').count('hash');
		expect(res[0].count).to.equal('8', 'should have stored 8 resources total');
	});

	it('can fetch the new webpage', async() => {
		let res = await Common.fetchStatic('res/'+res2.hash);
		let m = res.match(/stylesheet"/g);
		expect(m.length).to.equal(2);
		expect(res.match(/<img/)).to.not.equal(null);
	});

	it('can fetch info for editor', async() => {
		let res = await Common.request('resource/forhash', {
			hash: res2.hash
		}, 'POST');
		// console.log(res);
		expect(res.length).to.equal(3);
		expect(res[0].uri).to.equal('http://localhost:9994/index.html')
		expect(res[2].hash).to.equal(res2.hash);
		
	});

	it('can list snapshots by url', async() => {
		let res = await Common.request('resource/search', {
			url: 'http://localhost:9994/'
		}, 'POST');
		expect(res.count).to.equal(3);
		expect(res.entries.length).to.equal(3);
	});

	it('can remove snapshot by hash', async() => {
		let res = await Common.request('resource/remove', {
			hash: res2.hash
		}, 'POST');
		res = await Common.request('resource/forhash', {
			hash: res2.hash
		}, 'POST');
		expect(res.length).to.equal(0);
		let thrown = false;
		try {
			await Common.fetchStatic('res/'+res2.hash);
		} catch (e) {
			thrown = true;
			expect(e.response.status).to.equal(404);
		}
		expect(thrown).to.be.true;
	});

	it('can retrieve stats for resources', async() => {
		let stats = await Common.request('history/stats', null, 'post');
		// console.log(stats);
		expect(stats.resourceSize).to.be.greaterThan(100);
		expect(stats.resourcesByMime).to.deep.equal({
			'text/css': 3,
			'image/jpeg': 1,
			'text/plain': 1,
			'text/html': 2
		});
	})

	let res3;
	it('can store a second page', async() => {
		let html = await Common.fetchStatic('page1.html', 9994);
		res3 = await Common.request('resource/store', {
			uri: 'http://localhost:9994/page1.html',
			mime: 'text/html',
			content: html
		}, 'POST');
		let res = await connection('resource_content').count('hash');
		expect(res[0].count).to.equal('8', 'should have stored 8 resources total');
	});

	it('can fetch the new webpage', async() => {
		let res = await Common.fetchStatic('res/'+res3.hash+'?resolveLocal=latest');
		console.log(res);
	});

});
