import * as dbutil from '../../src/db/util.js';
import connection from '../../src/db/base.js';
import axios, { AxiosRequestConfig } from 'axios';
import http from 'http';
import createApp from '../../src/server.js';
import koaStatic from 'koa-static';
import Koa from 'koa';

const port = 9993;

let server: http.Server;
export async function setupOnce() {
	const dburi = new URL(process.env.DATABASE_URL);
	await dbutil.recreateDb(dburi);
	await dbutil.runMigrations(dburi);
}

export function startServer() {
	return new Promise(res => {
		// console.log('creating server with', process.env);
		const App = createApp();
		server = http.createServer(App.callback());
		server.listen(port, null, 0, () => {
			res(true);
		});
	});
}

export async function stopServer() {
	await server.close();
	server = null;
}

export async function teardownOnce() {
	
	connection.destroy();
}

export async function request(path, params, method, user?) {
	let opts: AxiosRequestConfig = {
		url: `http://localhost:${port}/api/${path}`,
		data: params,
		method,
		headers: {}
	};
	if (user) {
		opts.headers[process.env.USER_HEADER] = user;
	}
	let res = await axios(opts);
	if (res.status > 299) throw new Error('bad status code: '+res.statusText+' '+res.status);
	let body = res.data;
	if (body.ok !== true) throw new Error('response not okay - '+body.message);
	return body.data;
}
export async function fetchStatic(path, port=9993, binary=false) {
	let url = `http://localhost:${port}/${path}`;
	let opts: AxiosRequestConfig = {
		responseType: binary ? 'arraybuffer' : 'text'
	};
	let res = await axios.get(url, opts);
	if (res.status > 299) throw new Error('bad status code: '+res.statusText+' '+res.status);
	return res.data;
}

let staticServer: http.Server;
export async function startStaticServer() {
	if (staticServer) throw new Error('static server already running');
	await new Promise(res => {
		let app = new Koa();
		app.use(koaStatic('test/fixtures/server1'));
		staticServer = http.createServer(app.callback());
		staticServer.listen(9994, null, 0, () => {
			res(true);
		});
	});
}

export async function stopStaticServer() {
	if (!staticServer) return;
	await staticServer.close();
	staticServer = null;
}

export function sleep(ms) {
	return new Promise(res => {
		setTimeout(res, ms);
	});
}
