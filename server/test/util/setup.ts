import env from 'dotenv';
env.config({ path: '.env.local' });
env.config({ path: '.env' });
process.env.USER_WHITELIST = '';
process.env.USER_MODE = '';
