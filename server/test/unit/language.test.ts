import { describe, it } from 'mocha';
import { expect } from 'chai';
import * as Service from '../../src/service/language.js';

describe('Language', () => {

	it('can work', () => {
		let res = Service.getPostgresLanguage('en');
		expect(res).to.equal('english');
	});

	it('will use text detection over set locale', () => {
		let res = Service.getBestLanguage('this is an english text', 'de');
		expect(res).to.equal('english');
	})

	it('will use text detection over set locale', () => {
		let res = Service.getBestLanguage('this is an english text', 'de');
		expect(res).to.equal('english');
	})

	it('will use fallback if text is gibberish', () => {
		let res = Service.getBestLanguage('asdlhdwdpdw;df[owad', 'de');
		expect(res).to.equal('german');
	})

	it('can shorten locales', () => {
		let res = Service.getBestLanguage('', 'de-DE');
		expect(res).to.equal('german');
		res = Service.getBestLanguage('', 'en_UK');
		expect(res).to.equal('english');
	})

});
