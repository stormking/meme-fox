import axios from 'axios';
import Cheerio from 'cheerio';

const cache = new Map<string, Resource>();
const BaseUrl = 'http://localhost:3000/';

class Resource {
    public originUrl: string;
    public content: string;
    public mime: string;
    public local: string;
}

export function storeResource(url, content, mime): string {
    let local = getLocalUrl(url);
    let res = new Resource();
    res.originUrl = url;
    res.content = content;
    res.mime = mime;
    res.local = local;
    cache.set(url, res);
    console.log('stored resource', url, local);
    return local;
}


function getLocalUrl(url: string): string {
    let target = new URL(BaseUrl);
    target.pathname = encodeURIComponent(url);
    return target.href;
}

export function getStoreFromLocalUrl(url: string): Resource {
    let id = decodeURIComponent(url);
    console.log('looking up res', id);
    return cache.get(id);
}

async function getAndStoreResource(target, referer): Promise<string> {
    console.log('getAndStore', target, referer);
    let url = new URL( target, referer );
    url.hash = '';
    let local = getLocalUrl(url.href);
    if (url.href === referer) return local;
    if (!cache.has(url.href)) {
        let response = await axios.get(url.href, { headers: { referer } });
        let data;
        // console.log('headers', response.headers);
        let ct = response.headers['content-type'];
        if (ct && ct.includes('text/css')) {
            data = await prepCssForStorage(url.href, response.data)
        } else {
            data = response.data;
        }
        let res = new Resource();
        res.originUrl = url.href;
        res.content = data;
        res.mime = ct;
        res.local = local;
        cache.set(url.href, res);
    }
    console.log('stored resource', url.href, local);
    return local;
}

export async function prepCssForStorage(url, css): Promise<string> {
    return replaceUrlsInCss(css, async (target) => {
        let local = await getAndStoreResource(target, url);
        return local;
    })
}

export async function prepHtmlForStorage(baseUrl, html): Promise<string> {
    const $ = Cheerio.load(html);
	$('html').attr('xmlns', null);
	$('script').remove();
	// if (removeIframes) $('iframe').remove();
	
    let stylesheets = [];
    $('link[rel=stylesheet]').each((i, elem) => {
        stylesheets.push( $(elem) );
    });
    for (let stylesheet of stylesheets) {
        let href = stylesheet.attr('href');
        if (href) {
            let local = await getAndStoreResource(href, baseUrl);
            stylesheet.attr('href', local);
        }
    }
    let images = [];
	$('img').each((i, elem) => {
        images.push( $(elem) );
    })
    for (let img of images) {
        let src = img.attr('src');
        if (src) {
            let local = await getAndStoreResource(src, baseUrl);
            img.attr('src', local);
        }
    }
	return $.html();
}

export async function storeHtml(url, content) {
    let changed = await prepHtmlForStorage(url, content);
    let local = storeResource(url, changed, 'text/html');
    return local;
}

async function replaceUrlsInCss(buffer: string, hook: Function): Promise<string> {
	let placeholders = [];
	let placeholderCount = 0;
	let addPlaceholder = function(arg: string){
		let ph = "#~#"+(placeholderCount++)+"#~#";
		placeholders.push( hook(arg) );
		return ph;
	};
	let firstPass = buffer.replace(
        /(\burl\s*\(\s*(['"]?)(.+?)\2\s*\))|(@import\s*(['"])(.+?)\5)/ig,
		function(all,n1,n2,url,n4,n5,imp){
			if (!url && !imp) return all;
			if (url) {
				if (!n2) n2 = '';
				url = addPlaceholder( url );
				return `url(${n2}${url}${n2})`;
			}
			if (imp) {
				imp = addPlaceholder( imp );
				if (!n4) n4 = '"';
				return `@import ${n5}${imp}${n5}`;
			}
		}
	);
	console.debug("replace-urls",buffer.substr(0, 100),firstPass.substr(0, 100),placeholders);
	if (placeholders.length === 0) {
		return Promise.resolve(buffer);
	}
	try {
	    const placeholders_1 = await Promise.all(placeholders);
	    let secondPass = firstPass;
	    for (let i = 0; i < placeholders_1.length; i++) {
	      let ph_1 = "#~#" + i + "#~#";
	      secondPass = secondPass.replace(ph_1, placeholders_1[i]);
	    }
	    return secondPass;
	} catch (err) {
		console.debug(err, err.stack ? err.stack.split("\n") : '');
		return buffer;
	}
};