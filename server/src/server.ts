import Koa from 'koa';
import parse from 'co-body';

import Router from '@koa/router';
import HistoryController from './controller/history.js';
import ResourceController from './controller/resource.js';
import StaticController from './controller/static.js';
import InfoController from './controller/info.js';
import { middleware } from './service/user.js';

export default function create() {

	const app = new Koa();

	app.use(async (ctx, next) => {
		try {
			await next();
		} catch (err) {
			ctx.status = err.status || 500;
			if (ctx.request.header.accept === 'application/json') {
				ctx.body = JSON.stringify({ error: err.message });
			} else {
				ctx.body = err.message;
			}
			ctx.app.emit('error', err, ctx);
		}
	});

	app.use(async (ctx, next) => {
		const start = Date.now();
		await next();
		const ms = Date.now() - start;
		ctx.set('X-Response-Time', `${ms}ms`);
	});

	app.use(async (ctx,next) => {
		let cl = ~~ctx.request.header['content-length'];
		if (cl > 0) {
			Object.defineProperty(ctx.request, 'body', {
				value: await parse.json(ctx, { limit: '100Mb' })
			});
		}
		await next();
	});

	app.use(async (ctx,next) => {
		await next();
		if ('json' in ctx) {
			ctx.response.body = JSON.stringify({ ok: true, data: ctx.json });
		}
	});

	const ApiRouter = new Router({ prefix: '/api' });
	ApiRouter.use(middleware());
	ApiRouter.use('', HistoryController.routes());
	ApiRouter.use('', ResourceController.routes());
	ApiRouter.use('', InfoController.routes());
	const StaticRouter = new Router();
	StaticRouter.use(middleware());
	StaticRouter.use('', StaticController.routes());

	app.use(ApiRouter.routes())
		.use(StaticRouter.routes());


	app.on('error',(err,ctx) => {
		console.log(`Error on ${ctx.request.url}`, err);
	});
	return app;
}
