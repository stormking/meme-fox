import Debug from 'debug';
const debug = Debug('app:resource:css');

export default async function transformCss(processResourceLink, buffer: string): Promise<string> {
	function hook(defaultMime: string) {
		return async function(url: string): Promise<string> {
			try {
				let local = await processResourceLink( url, defaultMime );
				return local;
			} catch (err) {
				debug(err, err.stack ? err.stack.split("\n") : '');
				return '';
			}
		}
	}
	const importHook = hook('text/css');
	const urlHook = hook('image/png');
	let transformed = await replaceUrls(buffer, importHook, urlHook);
	return transformed;
}

export async function replaceUrls(buffer: string, importHook: Function, urlHook: Function): Promise<string> {
	let placeholders = [];
	let placeholderCount = 0;
	let addPlaceholder = function(hook: Function, arg: string){
		let ph = "#~#"+(placeholderCount++)+"#~#";
		placeholders.push([hook, arg]);
		return ph;
	};
	let firstPass = buffer.replace(/(url\s*\(\s*(['"]?)(?!#)(.+?)\2\s*\))|(@import\s*(['"])(.+?)\5)/ig,
		function(all,n1,n2,url,n4,n5,imp){
			if (!url && !imp) return all;
			if (url) {
				if (!n2) n2 = '';
				url = addPlaceholder( urlHook, url );
				return `url(${n2}${url}${n2})`;
			}
			if (imp) {
				imp = addPlaceholder( importHook, imp );
				if (!n4) n4 = '"';
				return `@import ${n5}${imp}${n5}`;
			}
		}
	);
	debug("replace-urls",buffer.substr(0, 100),firstPass.substr(0, 100),placeholders);
	if (placeholders.length === 0) {
		return Promise.resolve(buffer);
	}
	try {
		const placeholders_1 = [];
		for (let [hook, arg] of placeholders) {
			let res = await hook(arg);
			placeholders_1.push(res);
		}
	    // const placeholders_1 = await Promise.all(placeholders);
	    let secondPass = firstPass;
	    for (let i = 0; i < placeholders_1.length; i++) {
	      let ph_1 = "#~#" + i + "#~#";
	      secondPass = secondPass.replace(ph_1, placeholders_1[i]);
	    }
	    return secondPass;
	} catch (err) {
		debug(err, err.stack ? err.stack.split("\n") : '');
		return buffer;
	}
};
