import transformCss from './transform-css.js';
import Debug from 'debug';
const debug = Debug('app:resource:html');

const builtIn = {

	'a.href': {
		selector: 'a[href]',
		attribute: 'href',
		action: 'absolute'
	},

	'meta-headers': {
		selector: 'meta[http-equiv]',
		action: 'remove'
	},

	'link.stylesheet': {
		selector: 'link',
		filter: (a) => a.attr('rel') && a.attr('rel').toLowerCase() === 'stylesheet' && a.attr('href') && a.attr('href').length,
		attribute: 'href',
		mime: 'text/css'
	},

	'link.script': {
		selector: 'link',
		filter: (a) => a.attr('rel') && a.attr('rel').toLowerCase() === 'preload' && a.attr('href') && a.attr('href').length && a.attr('as') && a.attr('as').toLowerCase() === 'script',
		attribute: 'href',
		mime: 'application/javascript',
		action: 'remove'
	},

	'img.src': {
		selector: 'img[src]',
		attribute: 'src',
		mime: 'image/jpeg'
	},

	'img.srcset': {
		selector: 'img[srcset]',
		action: 'exec',
		exec: async (elem, processResourceLink) => {
			let res = [];
			let parts = elem.attr('srcset').split(',');
			for (let row of parts) {
				let parts = row.trim().split(' ');
				let url = parts[0].trim();
				parts[0] = await processResourceLink( url, 'image/jpeg' );
				res.push(parts.join(' '))
			}
			elem.attr('srcset',res.join(', '));
		}
	},

	'script': {
		selector: 'script',
		action: 'remove'
	},


	'inlinestyle': {
		selector: '[style]',
		filter: (e) => e.attr('style').match(/url\(/),
		action: 'exec',
		exec: async(elem, processResourceLink) => {
			let style = elem.attr('style');
			style = await updateStyles(style, processResourceLink);
			elem.attr('style',style);
		}
	},

	'docstyle': {
		selector: 'style',
		action: 'exec',
		exec: async(elem, processResourceLink) => {
			let style = elem.html();
			style = await updateStyles(style, processResourceLink);
			elem.html(style);
		}
	}
};

function updateStyles(text: string, processResourceLink) {
	debug("update inline style",text);
	return transformCss( processResourceLink, text );
};

export interface Rule {
	selector: string,
	action?: 'exec'|'process'|'remove',
	exec?: Function,
	mime?: string & Function,
	filter?: Function,
	attribute?: string,
	resource?: boolean
}

export default function build(): Rule[] {
	let index: { [x: string]: Rule } = {};
	for (let k in builtIn) {
		index[k] = builtIn[k];
	}
	return Object.values(index).filter(entry => {
		if (typeof entry !== 'object') return false;
		return true;
	});
}
