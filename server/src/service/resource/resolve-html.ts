import * as ResourceRepo from '../../db/resource.js';
import axios, { ResponseType } from 'axios';
import Debug from 'debug';
const debug = Debug('app:resource');
import cheerio from 'cheerio';
import build from "./html-filters.js";
import Mime from 'mime';
import transformCss from './transform-css.js';

export default async function resolveHtml(buffer: string, method: string) {
	const dom = cheerio.load(buffer);
	const callbacks = build();
	const urlCache = new Map();
	
	const checkResolve = async(url) => {
		if (!urlCache.has(url)) {
			// console.log('check local', url);
			urlCache.set(url, checkStorageForLocal(url, method));
		}
		return urlCache.get(url);
	}

	const run = async(elem, instruction) => {
		if (instruction.filter && !instruction.filter(elem)) return;
		switch (instruction.action) {
			case 'absolute': 
				let href = elem.attr(instruction.attribute);
				if (href.substring(0,4) !== 'http') return href;
				let resolved = await checkResolve(href);
				if (resolved !== href) {
					elem.attr(instruction.attribute, resolved);
				}
			break;
		}
	};
	let wait = [];
	for (let instruction of callbacks) {
		let elems = dom(instruction.selector);
		let list = [];
		elems.each(function() {
			list.push(dom(this));
		});
		for (let elem of list) {
			wait.push(run(elem, instruction));
		}
	}
	await Promise.all(wait);
	return dom.html();
}

async function checkStorageForLocal(url: string, method: string) {
	let content = await ResourceRepo.getResourceByUri(url);
	if (content && content.length) {
		let hash, mime;
		if (method === 'latest') ({ hash, mime } = content[0]);
		else ({ hash, mime } = content[content.length-1]);
		let fname = `${hash}.${Mime.getExtension(mime)}?resolveLocal=${method}`;
		return fname;
	} else {
		return url;
	}
}
