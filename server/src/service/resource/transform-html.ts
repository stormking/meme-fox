import * as ResourceRepo from '../../db/resource.js';
import axios, { ResponseType } from 'axios';
import Debug from 'debug';
const debug = Debug('app:resource');
import cheerio from 'cheerio';
import build from "./html-filters.js";
import Mime from 'mime';
import transformCss from './transform-css.js';

export default async function transformHtml(buffer: string, url: string, styleInsert?: string, blobs?) {
	const dom = cheerio.load(buffer);
	const callbacks = build();
	const wait = [];
	const urlCache = new Map();
	const process = processResourceLink(url, urlCache);
	const blobMap = new Map();
	if (blobs && blobs.length) {
		for (let { link, datauri } of blobs) {
			let hash = await storeBlob(datauri, link);
			blobMap.set(link, hash);
		}
	}
	const run = async(elem, instruction) => {
		if (instruction.filter && !instruction.filter(elem)) return;
		switch (instruction.action) {
			default:
			case 'process':
				let value = elem.attr(instruction.attribute);
				let fbmime;
				if (instruction.mime.call) {
					let e = <any> elem['0'];
					fbmime = instruction.mime(e.attribs);
				} else {
					fbmime = ""+instruction.mime;
				}
				if (blobMap.has(value)) {
					value = blobMap.get(value);
				} else {
					value = await process(value, fbmime);
				}
				if (!value) {
					elem.remove();
				} else {
					elem.attr(instruction.attribute, value);
				}
			break;

			case 'remove':
				elem.remove();
			break;

			case 'exec':
				await instruction.exec(elem, process);
			break;

			case 'absolute': 
				let href = elem.attr(instruction.attribute);
				if (href.substring(0,5) === 'blob:' || href.substring(0,5) === 'data:' || href.substring(0,7) === 'mailto:' || href.substring(0,11) === 'javascript:') {
					return href;
				}
				let normalized = new URL( href, url );
				elem.attr(instruction.attribute, normalized.href);
			break;
		}
	};
	for (let instruction of callbacks) {
		let elems = dom(instruction.selector);
		let list = [];
		elems.each(function() {
			list.push(dom(this));
		});
		for (let elem of list) {
			await run(elem, instruction);
		}
	}
	if (styleInsert) {
		let fname = await storeBlob(`data:text/css,${styleInsert}`, `${url}_styles.css`);
		dom('head').append(`<link rel="stylesheet" href="${fname}" />`);
	}
	dom('meta[ref="memefox-original"]').remove();
	dom('head').append('<meta ref="memefox-original" href="'+url+'" />')
	// callbacks.forEach((instruction: Rule) => {
	// 	let elems = dom(instruction.selector);
	// 	elems.each(function() {
	// 		wait.push((run)(dom(this), instruction));
	// 	})
	// });
	await Promise.all(wait);
	return dom.html();
}

async function storeBlob(dataUrl: string, url: string): Promise<string> {
	let split = dataUrl.indexOf(',');
	let meta = dataUrl.substring(5, split);
	let data = dataUrl.substring(split+1);
	let encSplit = meta.indexOf(';');
	let mime, encoding;
	if (encSplit > -1) {
		mime = meta.substring(0, encSplit);
		encoding = meta.substring(encSplit+1);
	} else {
		mime = meta;
	}
	// console.log('storing blob', url);
	let res = await ResourceRepo.store(mime, url, Buffer.from(data, encoding), null);
	let fname = `${res.hash}.${Mime.getExtension(mime)}`;
	return fname;
}

export function processResourceLink(referer: string, cache: Map<string, string>) {
	return async function (url: string, mime: string): Promise<string> {
		if (url.substring(0,5) === 'blob:' || url.substring(0,5) === 'data:' || url.substring(0,7) === 'mailto:' || url.substring(0,11) === 'javascript:') {
			return url;
		}
		let normalized = new URL( url, referer );
    	normalized.hash = '';
		if (cache.has(normalized.href)) {
			return cache.get(normalized.href);
		}
		cache.set(normalized.href, null);	//recursion breaker
		try {
			let hash = await getAndStoreResource(url, referer, mime, processResourceLink, cache);
			let fname = `${hash}.${Mime.getExtension(mime)}`;
			cache.set(normalized.href, fname);
			return fname;
		} catch (e) {
			console.log('error getting resource', e.message, normalized.href);
			return null;
		}
	}
}

export async function getAndStoreResource(target: string, referer: string, mime: string, process, cache): Promise<string> {
    debug('getAndStore', target, referer);
    let url = new URL( target, referer );
    url.hash = '';
	let existing = await ResourceRepo.getResourceByUri(url.href);
	if (existing.length) {
		// console.log(url.href, 'already stored');
		return existing[0].hash;
	}
    const responseType: ResponseType = 'arraybuffer';
	// console.log('fetching ', url.href);
	let response = await axios.get(url.href, { headers: { referer }, responseType });
	// console.log('got data', response);
	let data;
	// console.log('headers', response.headers);
	let ct = response.headers['content-type'];
	let ctmime;
	if (ct) {
		ctmime = ct.split(';')[0];
	}
	mime = ctmime || mime;
	if (mime.includes('text/css')) {
		data = Buffer.from(await transformCss(process(url.href, cache), response.data.toString('utf8')))
	} else {
		data = response.data;
	}
	let local = await ResourceRepo.store(ctmime || mime, url.href, data, null);
    // console.log('stored resource', url.href, local);
    return local.hash;
}
