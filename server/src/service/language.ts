import { detect } from 'tinyld';
import iso from 'iso-639-1';

const allowed = [	//SELECT cfgname FROM pg_ts_config
	// 'simple',
	'arabic',
	'danish',
	'dutch',
	'english',
	'finnish',
	'french',
	'german',
	'greek',
	'hungarian',
	'indonesian',
	'irish',
	'italian',
	'lithuanian',
	'nepali',
	'norwegian',
	'portuguese',
	'romanian',
	'russian',
	'spanish',
	'swedish',
	'tamil',
	'turkish'
];

const isoIndex = Object.fromEntries(allowed.map(name => {
	let code = iso.getCode(name[0].toLocaleUpperCase()+name.substr(1));
	if (!code) {
		console.warn('no code for language: '+name);
		return null;
	}
	return [code, name];
}).filter(e => !!e));

export function getPostgresLanguage(mainLocale: string, fallbackLocale?: string): string {
	let main = isoIndex[mainLocale];
	if (main) return main;
	let fb = fallbackLocale ? isoIndex[fallbackLocale] : null;
	if (fb) return fb;
	return 'simple';
}

export function detectLanguageFromText(text: string): string {
	return detect(text);
}

const longLocale = /^[a-z]{2}(-|_)[A-Z]{2}/;
export function getBestLanguage(text: string, locale: string) {
	let textLocale = detectLanguageFromText(text);
	if (longLocale.test(locale)) {
		locale = locale.substr(0, 2);
	}
	let pg = getPostgresLanguage(textLocale, locale);
	return pg;
}
