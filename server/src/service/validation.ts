import AJV, { ValidateFunction } from 'ajv';
import Formats from 'ajv-formats';
import HistoryAdd from './validation/history-add.json';
import HistoryUpdateBookmarked from './validation/history-updatebookmarked.json';
import HistorySearch from './validation/history-search.json';
import ResourceStore from './validation/resource-store.json';
import ResourceInfoForUrl from './validation/resource-forurl.json';
import ResourceInfoForHash from './validation/resource-forhash.json';
import ResourceSearch from './validation/resource-search.json';
import ResourceRemove from './validation/resource-remove.json';

const ajv = new AJV({
	allErrors: true
});

Formats(ajv);

let compiled = new Map<string, ValidateFunction>();
[
	HistoryAdd,
	HistoryUpdateBookmarked,
	HistorySearch,
	ResourceStore,
	ResourceInfoForUrl,
	ResourceInfoForHash,
	ResourceRemove,
	ResourceSearch
].forEach(json => {
	let test = ajv.compile(json);
	compiled.set(json.$id, test);
})

export default function validate(schema: string, data: any) {
	if (!compiled.has(schema)) throw new Error('unknown validation: '+schema);
	let test = compiled.get(schema);
	let res = test(data);
	if (res) return;
	let errors = [];
	test.errors.forEach(e => {
		// console.log(e);
		let str = e.message;
		if (e.instancePath) str += ' - at '+e.instancePath;
		str += ' ('+Object.entries(e.params).map(([key, val]) => {
			return `${key}: ${val}`
		}).join(', ')+')';
		errors.push(str);
	});
	let msg = 'Validation failed\n  > ';
	msg += errors.join('\n  > ');
	throw new Error(msg);
}
