import * as ResourceRepo from '../db/resource.js';
import transformHtml from './resource/transform-html.js';
import resolveHtml from './resource/resolve-html.js';
import { getInvertedUrl } from './search.js';


export async function store(uri: string, mime: string, content: Buffer, changes, styleInsert?: string, blobs?, isSnapshot?: boolean) {
	if (mime === 'text/html' && !isSnapshot) {
		// let changed = await prepHtmlForStorage(uri, content, styleInsert);
		let changed = await transformHtml(content.toString('utf8'), uri, styleInsert, blobs);
		content = Buffer.from(changed);
	}
	return ResourceRepo.store(mime, uri, content, changes);
}

export async function fetchContent(hash: string, resolveLocal?: string) {
	let res = await ResourceRepo.getContentByHash(hash, true);
	if (resolveLocal) {
		res.content = await resolveHtml(res.content, resolveLocal);
	}
	return res;
}

export async function getInfoForUrl(url: string) {
	let entries = await ResourceRepo.getResourceByUri(url);
	return entries.map(e => ({
		uid: e.uid,
		hash: e.hash,
		created: e.created
	}));
}

export async function getInfoForHash(hash: string) {
	let contentInfo = await ResourceRepo.getContentByHash(hash, false);
	if (!contentInfo) return [];
	let metaInfos = await ResourceRepo.getResourcesWithIdenticalUrlByContentId(contentInfo.uid)
	return metaInfos;
}

export async function search({ url, createdBefore, createdAfter, sortCol, sortDir, offset, limit }) {
	if (createdAfter) createdAfter = new Date(createdAfter);
	if (createdBefore) createdBefore = new Date(createdBefore);
	if (url) url = getInvertedUrl(url, true);
	let count = await ResourceRepo.count(url, createdAfter, createdBefore);
	let entries = [];
	if (count > 0) {
		entries = await ResourceRepo.search(
			url,
			createdAfter,
			createdBefore,
			sortCol || 'uri',
			sortDir || 'asc',
			offset || 0,
			limit || 10
		);
		entries = entries.map(e => ({
			uid: e.uid,
			uri: e.uri,
			created: e.created,
			hash: e.hash
		}));
	}
	return {
		count, entries
	}
}

export async function remove(hash: string) {
	let metaRows = await ResourceRepo.getMetaByHash(hash);
	let ids = metaRows.map(e => e.uid);
	await ResourceRepo.removeMetaByIds(ids);
	await ResourceRepo.removeContentByHash(hash);
	return metaRows;
}
