import { transliterate } from 'transliteration';

export function makeSearch(terms: string[]): Set<string> {
	let all = terms.join(' ').toLowerCase();
	let filtered = all.replace(/[^\d\w]+/g, ' ');
	let translit = transliterate(filtered);
	let items = new Set(translit.split(' ').filter(e => e.length > 2));
	return items;
}

export function getInvertedUrl(url: string, query=false): string {
	let u = new URL(url);
	let host = u.hostname.split('.').reverse().join('.');
	let wildcard = query ? '%' : '';
	let inverse = `${host}${wildcard}${u.pathname}${wildcard}`;
	return inverse;
}
