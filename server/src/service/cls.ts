import { AsyncLocalStorage } from "async_hooks";

const CLS = new AsyncLocalStorage();

export function run(fn: () => unknown) {
	let storage = new Map();
	return CLS.run(storage, fn);
}

export function set(key, val) {
	let storage: any = CLS.getStore();
	if (!storage) return;
	storage.set(key, val);
}

export function get(key) {
	let storage: any = CLS.getStore();
	if (!storage) return;
	return storage.get(key);
}
