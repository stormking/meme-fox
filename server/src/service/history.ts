import * as HistoryRepo from '../db/history.js';
import { makeSearch, getInvertedUrl } from './search.js';
import { getBestLanguage, detectLanguageFromText } from './language.js';

export async function addHistory(url, title, description, textContent, locale, headlines, keywords, bookmarked) {
	let search = [
		makeSearch([title, description]),
		makeSearch((keywords || '').split(',')),
		makeSearch([headlines]),
		makeSearch([textContent])
	].map(e => Array.from(e).join(' '));
	let lang = getBestLanguage(`${title} ${description} ${textContent}`, locale);
    let uid = await HistoryRepo.upsert(url, title, description, search, lang, bookmarked);
	return uid;
}

export async function updateBookmarked(url, bookmarked) {
	return HistoryRepo.updateBookmarked(url, bookmarked);
}

export async function getForUrl(url) {
	return HistoryRepo.getForUrl(url);
}

export async function searchHistory({ query, offset, limit, locale, bookmarked, lastVisitedAfter, lastVisitedBefore, firstVisitedBefore, firstVisitedAfter, url }) {
	let queryItems: Set<string> = query && query.length ? makeSearch([query]) : new Set();
	// console.log(queryItems);
	let lang = getBestLanguage(query, locale);
	if (firstVisitedBefore) firstVisitedBefore = new Date(firstVisitedBefore);
	if (firstVisitedAfter) firstVisitedAfter = new Date(firstVisitedAfter);
	if (lastVisitedAfter) lastVisitedAfter = new Date(lastVisitedAfter);
	if (url) url = getInvertedUrl(url, true);
	let count = await HistoryRepo.searchCount(queryItems, lang, bookmarked, lastVisitedAfter, lastVisitedBefore, firstVisitedBefore, firstVisitedAfter, url);
	let entries = [];
	if (count > 0) {
		entries = await HistoryRepo.search(queryItems, offset, limit, lang, bookmarked, lastVisitedAfter, lastVisitedBefore, firstVisitedBefore, firstVisitedAfter, url);
	}
	return { count, entries };
}

export async function stats() {
	return HistoryRepo.stats();
}
