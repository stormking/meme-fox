import * as CLS from './cls.js';

const GuestRoutes = [
	'/api/info'
];

export function middleware() {
	const UserMode = process.env.USER_MODE;
	const RequireUser = ['simple', 'oidc'].includes(UserMode);
	const UserHeaderKey = process.env.USER_HEADER || 'x-user';
	const UserWhitelist = process.env.USER_WHITELIST ? process.env.USER_WHITELIST.split(',') : null;
	return async(ctx, next) => {
		await CLS.run(async() => {
			CLS.set('ctx', ctx);
			let headers = ctx.request.header;
			// console.log(ctx.request);
			if (RequireUser && !(UserHeaderKey in headers) && !GuestRoutes.includes(ctx.request.url)) {
				ctx.response.status = 401;
				ctx.response.body = 'not authenticated';
				return;
			}
			let user = headers[UserHeaderKey];
			if (RequireUser && user && UserWhitelist && !UserWhitelist.includes(user) && !GuestRoutes.includes(ctx.request.url)) {
				ctx.response.status = 403;
				ctx.response.body = 'not authorized';
				return;
			}
			if (!RequireUser) {
				user = 'root';
			}
			CLS.set('user', user);
			await next();
		});
	}
}

export function getUser() {
	let user = CLS.get('user');
	return user || null;
}

export function getContext() {
	return CLS.get('ctx');
}
