import Router from '@koa/router';
import * as Service from '../service/history.js';
import validate from '../service/validation.js';

const router = new Router({
	prefix: '/history'
});

router.post('/add',
	async(ctx) => {
		let req: any = ctx.request;
		let params = req.body;
		validate('history/add', params);
		let uid = await Service.addHistory(params.url, params.title, params.description || '', params.textContent || '', params.locale || 'en', params.headlines || '', params.keywords || '', params.bookmarked || false);
		ctx.json = { history: uid };
	}
);

router.post('/updateBookmarked',
	async(ctx) => {
		let req: any = ctx.request;
		let params = req.body;
		validate('history/updateBookmarked', params);
		let uid = await Service.updateBookmarked(params.url, params.bookmarked);
		ctx.json = { history: uid };
	}
);

router.post('/search',
	async(ctx) => {
		let req: any = ctx.request;
		let params = req.body;
		validate('history/search', params);
		let res = await Service.searchHistory(params);
		ctx.json = res;
	}
);

router.post('/stats',
	async(ctx) => {
		let res = await Service.stats();
		ctx.json = res;
	}
);

export default router;
