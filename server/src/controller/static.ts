import Router from '@koa/router';
import * as Service from '../service/resource.js';
import validate from '../service/validation.js';

const router = new Router({
	prefix: '/res'
});

router.get('/:hash',
	async(ctx) => {
		let hash: any = ctx.params.hash;
		if (hash.indexOf('.') > 0) hash = hash.substring(0, hash.indexOf('.'));
		if (hash.length !== 40) {
			ctx.status = 400;
			ctx.response.body = 'invalid parameter, not a hash';
			return;
		}
		let resolveLocal = null;
		if (ctx.query && ctx.query.resolveLocal) {
			let q: string = Array.isArray(ctx.query.resolveLocal) ? ctx.query.resolveLocal[0] : ctx.query.resolveLocal;
			if (['first', 'latest'].includes(q)) {
				resolveLocal = ctx.query.resolveLocal;
			}
		}
		let res = await Service.fetchContent(hash, resolveLocal);
		if (!res) {
			ctx.status = 404;
			ctx.response.body = '404';
		} else {
			ctx.response.set('Content-Type', res.mime);
			ctx.response.set('Content-Length', res.size);
			ctx.response.body = res.content;
		}
	}
);

export default router;
