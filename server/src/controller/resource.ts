import Router from '@koa/router';
import * as Service from '../service/resource.js';
import validate from '../service/validation.js';

const router = new Router({
	prefix: '/resource'
});

router.post('/store',
	async(ctx) => {
		let req: any = ctx.request;
		let params = req.body;
		validate('resource/store', params);
		let resource = await Service.store(params.uri, params.mime, Buffer.from(params.content), params.changes, params.styleInsert, params.blobs, params.isSnapshot);
		ctx.json = resource;
	}
);

router.post('/remove',
	async(ctx) => {
		let req: any = ctx.request;
		let params = req.body;
		validate('resource/remove', params);
		let resource = await Service.remove(params.hash);
		ctx.json = resource;
	}
);

router.post('/forurl',
	async(ctx) => {
		let req: any = ctx.request;
		let params = req.body;
		validate('resource/forurl', params);
		let resource = await Service.getInfoForUrl(params.uri);
		ctx.json = resource;
	}
);

router.post('/forhash',
	async(ctx) => {
		let req: any = ctx.request;
		let params = req.body;
		validate('resource/forhash', params);
		let resource = await Service.getInfoForHash(params.hash);
		ctx.json = resource;
	}
);

router.post('/search',
	async(ctx) => {
		let req: any = ctx.request;
		let params = req.body;
		validate('resource/search', params);
		let resource = await Service.search(params);
		ctx.json = resource;
	}
);

export default router;
