import Router from '@koa/router';

const router = new Router({});

router.get('/info',
	async(ctx) => {
		let userMode = process.env.USER_MODE;
		let userHeader = userMode === 'simple' ? process.env.USER_HEADER : undefined;
		let userWhitelist = userMode === 'simple' ? !!process.env.USER_WHITELIST : undefined;
		ctx.json = {
			memefox: 'v0.5',
			userMode,
			userWhitelist,
			userHeader
		};
	}
);

export default router;
