import connection from './base.js';
import { getInvertedUrl } from '../service/search.js';
import { createHash } from 'crypto';
import { getUser } from '../service/user.js';

export async function getResourceByUri(uri: string) {
	let user = getUser();
	let res = await connection('resource_meta').select(['resource_meta.uid', 'uri', 'created', 'changes', 'hash', 'mime', 'size'])
		.join('resource_content', 'resource_meta.content', 'resource_content.uid')
		.where({ 'resource_meta.uri': uri, user })
		.orderBy('created', 'desc');
	return res;
}

export async function getResourcesWithIdenticalUrlByContentId(uid: number) {
	let user = getUser();
	let res = await connection('resource_meta AS b')
		.select(['o.*', 'c.hash'])
		.join('resource_meta AS o', 'b.uri', 'o.uri')
		.join('resource_content AS c', 'o.content', 'c.uid')
		.where({ 'b.content': uid, 'o.user': user })
	return res;
}

export async function getContentByHash(hash: string, includeBlob: boolean) {
	let select = includeBlob ? '*' : ['uid', 'size', 'mime', 'hash'];
	let res = await connection('resource_content').select(select).where({ hash }).limit(1);
	return res[0];
}

export async function getMetaByHash(hash: string) {
	let user = getUser();
	let res = await connection('resource_meta AS b')
		.select(['b.*'])
		.join('resource_content AS c', 'b.content', 'c.uid')
		.where({ 'c.hash': hash, 'b.user': user });
	return res;
}

export async function removeContentByHash(hash: string) {
	await connection('resource_content').where({ hash }).delete();
}
export async function removeMetaByIds(ids: number[]) {
	await connection('resource_meta').whereIn('uid', ids).delete();
}

export async function store(mime: string, uri: string, content: Buffer, changes: object) {
	const user = getUser();
	const hash = createHash('sha1').update(content).digest('hex');
	const existing = await getContentByHash(hash, false);
	let contentId: string;
	if (!existing) {
		contentId = await storeContent(content, hash, mime);
	} else {
		contentId = existing.uid;
	}
	const created = new Date().toJSON();
	let meta = await connection('resource_meta')
		.join('resource_content', 'resource_meta.content', 'resource_content.uid')
		.select(['resource_meta.uid', 'hash', 'created'])
		.where({ uri, 'resource_meta.content': contentId, user })
		.limit(1);
	if (meta.length) {
		return meta[0];
	}
	meta = await connection('resource_meta').insert({
		content: contentId,
		uri,
		user,
		urlInverse: getInvertedUrl(uri),
		created,
		changes: JSON.stringify(changes)
	}).returning('uid');
	return { uid: meta[0], hash, created };
}

async function storeContent(content: Buffer, hash: string, mime: string) {
	// console.log('storeContent', { mime, hash, content });
	let res = await connection('resource_content').insert({
		hash,
		mime,
		size: content.length,
		content
	}).returning('uid');
	return res[0];
}

export async function search(url: string, createdAfter: Date, createdBefore: Date, sortCol: string, sortDir: string, offset: number, limit: number) {
	const user = getUser();
	let q = connection('resource_meta AS m')
		.join('resource_content AS c', 'm.content', 'c.uid')
		.select(['m.*', 'c.hash'])
		.where('c.mime', '=', 'text/html')
		.where('m.user', user)
		.orderBy(sortCol, sortDir)
		.offset(offset)
		.limit(limit);
	if (url) q = q.where('urlInverse', 'LIKE', url);
	if (createdAfter) q = q.where('created', '>', createdAfter);
	else if (createdBefore) q = q.where('created', '<', createdBefore);
	let res = await q;
	return res;
}

export async function count(url: string, createdAfter: Date, createdBefore: Date): Promise<number> {
	const user = getUser();
	let q = connection('resource_meta AS m')
		.join('resource_content AS c', 'm.content', 'c.uid')
		.count('m.uid')
		.where('m.user', user)
		.where('c.mime', '=', 'text/html');
	if (url) q = q.where('urlInverse', 'LIKE', url);
	if (createdAfter) q = q.where('created', '>', createdAfter);
	else if (createdBefore) q = q.where('created', '<', createdBefore);
	let res: any = await q;
	return res[0].count * 1;
}
