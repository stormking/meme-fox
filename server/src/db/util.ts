import { exec } from 'child_process'
import Debug from 'debug';
const debug = Debug('app:db');

function pexec(cmd): Promise<void> {
	return new Promise(function(resolve, reject) {
		debug('run', cmd);
		let p = exec(cmd, {}, (err) => {
			if (err) reject(err);
			else resolve();
		});
	});
}

export function waitForDatabase(dbUri): Promise<void> {
	return new Promise(function(resolve, reject) {
		let cmd = `bash -c 'while !</dev/tcp/${dbUri.hostname}/${dbUri.port}; do sleep 1; done;'`;
		debug('wait', cmd);
		let p = exec(cmd, {}, (err) => {
			if (err) reject(err);
			else resolve();
		});
		setTimeout(() => {
			if (p.killed) return;
			p.kill();
			reject(new Error('timeout connecting to database'));
		},6000);
	});
}

export async function runMigrations(dbUri): Promise<void> {
	let cmd = `npx knex --migrations-directory migrations --client pg --connection ${dbUri.href} migrate:latest`;
	await pexec(cmd);
}

export async function recreateDb(dbUri): Promise<void> {
	let prefix = 'docker-compose -f docker-compose.dev.yaml exec -T db psql -U test -d postgres  -c ';
	let drop = prefix+'"DROP DATABASE test;"';
	let create = prefix+'"CREATE DATABASE test;"';
	await pexec(drop);
	await pexec(create);
}


