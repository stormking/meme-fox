import knex from 'knex';
import env from 'dotenv';

env.config({ path: '.env.local' });
env.config({ path: '.env' });

const connection = knex({
	client: 'pg',
	connection: process.env.DATABASE_URL,
	pool: {
		min: 0,
		max: 100,
		log: (message, logLevel) => console.log(`${logLevel}: ${message}`)
	}
})

export default connection;

process.on('SIGTERM', () => {
	connection.destroy();
});
