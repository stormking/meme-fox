import connection from './base.js';
import * as crypto from 'crypto';
import { getInvertedUrl } from '../service/search.js';
import { getUser } from '../service/user.js';

export async function upsert(url: string, title: string, description: string, search: string[], lang: string, bookmarked: boolean): Promise<number> {
	let [searchA, searchB, searchC, searchD] = search;
	let hash = crypto.createHash('sha1')
		.update(Buffer.from(`${title}${description}${searchD}`, 'utf8'))
		.digest('base64');
	let user = getUser();
	let inverse = getInvertedUrl(url);
	let result;
	await connection.transaction(async trx => {
		let meta = await connection.raw(`
			insert into history_meta ("hash", "title", "description", "search") 
			values (:hash, :title, :description, 
				setweight(to_tsvector(:lang, :searchA), 'A') 
				|| setweight(to_tsvector(:lang, :searchB), 'B') 
				|| setweight(to_tsvector(:lang, :searchC), 'C') 
				|| setweight(to_tsvector(:lang, :searchD), 'D')
			)
			on conflict ("hash") do nothing 
			returning "uid"`, 
			{ hash, title, description, lang, searchA, searchB, searchC, searchD })
			.transacting(trx);
		let uid;
		if (meta.rows.length > 0) {
			uid = meta.rows[0].uid;
		} else {
			let res = await connection('history_meta').select('uid').where({ hash }).limit(1);
			uid = res[0].uid;
		}

		let exists = await connection('history').select().where({ url, user });
		if (exists.length > 0) {
			let row = exists[0];
			let res = await connection('history').update({
				lastVisited: new Date(),
				timesVisited: row.timesVisited+1,
				meta: uid,
				bookmarked
			}).where({ uid: row.uid })
			.transacting(trx);
			result = row.uid;
		} else {
			let res = await connection('history').insert({
				url,
				urlInverse: inverse,
				firstVisited: new Date(),
				lastVisited: new Date(),
				timesVisited: 1,
				meta: uid,
				bookmarked,
				user
			}).returning('uid')
			.transacting(trx);
			result = res[0];
		}
	});
	return result;
    
}

export async function updateBookmarked(url: string, bookmarked: boolean) {
	let user = getUser();
	let res = await connection('history').update({
		bookmarked
	}).where({ url, user }).returning('uid');
	return res[0];
}

export async function search(query: Set<string>, offset: number, limit: number, lang: string, bookmarked: boolean, 
	lastVisitedAfter: Date, lastVisitedBefore: Date, firstVisitedBefore: Date, firstVisitedAfter: Date, url: string) {
	let queryStr = Array.from(query.values()).join(' | ');
	let user = getUser();
	let select = [
		'url', 'history_meta.title', 'history_meta.description', 'firstVisited', 'lastVisited', 'timesVisited'
	];
	let orderCol = 'lastVisited';
	if (queryStr.length) {
		let col: any = connection.raw('ts_rank_cd(history_meta.search, to_tsquery(:lang, :queryStr)) AS score', { lang, queryStr });
		select.push(col);
		orderCol = 'score';
	}
	let q = connection('history_meta')
		.join('history', 'history.meta', 'history_meta.uid')
		.select(select)
		.orderBy(orderCol, 'desc')
		.where('history.user', user)
		.offset(offset || 0)
		.limit(limit || 100);
	if (queryStr.length) {
		q = q.whereRaw('history_meta.search @@ (to_tsquery(:lang, :queryStr))', { lang, queryStr });
	}
	if (bookmarked) {
		q = q.where('history.bookmarked', true);
	}
	if (lastVisitedAfter) {
		q = q.where('lastVisited', '>', lastVisitedAfter);
	} else if (lastVisitedBefore) {
		q = q.where('lastVisited', '<', lastVisitedBefore);
	}
	if (firstVisitedBefore) {
		q = q.where('firstVisited', '<', firstVisitedBefore);
	} else if (firstVisitedAfter) {
		q = q.where('firstVisited', '>', firstVisitedAfter);
	}
	if (url) {
		q = q.where('urlInverse', 'LIKE', url);
	}
	let res = await q;
	return res;
}

export async function searchCount(query: Set<string>, lang: string, bookmarked: boolean, 
	lastVisitedAfter: Date, lastVisitedBefore: Date, firstVisitedBefore: Date, firstVisitedAfter: Date, url: string): Promise<number> {
	let queryStr = Array.from(query.values()).join(' | ');
	let user = getUser();
	let q = connection('history_meta')
		.join('history', 'history.meta', 'history_meta.uid')
		.where('history.user', user)
		.count('history.uid');
	if (queryStr.length) {
		q = q.whereRaw('history_meta.search @@ (to_tsquery(:lang, :queryStr))', { lang, queryStr });
	}
	if (bookmarked) {
		q = q.where('history.bookmarked', true);
	}
	if (lastVisitedAfter) {
		q = q.where('lastVisited', '>', lastVisitedAfter);
	}
	if (firstVisitedBefore) {
		q = q.where('firstVisited', '<', firstVisitedBefore);
	} else if (firstVisitedAfter) {
		q = q.where('firstVisited', '>', firstVisitedAfter);
	}
	if (url) {
		q = q.where('urlInverse', 'LIKE', url);
	}
	let res: any = await q;
	return res[0].count * 1;
}

export async function getForUrl(url: string) {
	let user = getUser();
	let res = await connection('history').select().where('url', url).where('user', user).limit(1);
	return res[0];
}

export async function cleanupOrphaned() {
	await connection('history_meta')
		.whereIn('uid', function() {
			this.select('history_meta.uid')
				.from('history_meta')
				.leftJoin('history', 'history.meta', 'history_meta.uid')
				.whereNull('history.uid');
		})
		.del();
}

export async function stats() {
	let historyMetaSize = await connection.raw("select pg_total_relation_size('history_meta')");
	let resourceContentSize = await connection.raw("select pg_total_relation_size('resource_content')");
	let resourceCount = await connection.raw('select mime, count(uid) from resource_content group by mime');
	let now = new Date().toJSON().substring(0, 10);
	let visitedToday = await connection('history')
		.where('lastVisited', '>', `${now} 00:00`)
		.count();
	let addedToday = await connection('history')
		.where('firstVisited', '>', `${now} 00:00`)
		.count();
	let total = await connection('history')
		.count();
	return {
		metaSize: ~~historyMetaSize.rows[0].pg_total_relation_size,
		resourceSize: ~~resourceContentSize.rows[0].pg_total_relation_size,
		resourcesByMime: resourceCount.rows.reduce((c, v) => {
			c[v.mime] = ~~v.count;
			return c;
		}, {}),
		visitedToday: ~~visitedToday[0].count,
		addedToday: ~~addedToday[0].count,
		total: ~~total[0].count
	};
}
