

export const up = async function(knex) {
    return Promise.all([

        // extracted searchable page data
        knex.schema.createTable('resource_meta', t => {
            t.increments('uid').primary();
			t.string('uri', 2048).notNullable();
			t.index(['uri'], 'idx_resource_uri');
			t.datetime('created').notNullable();
			t.integer('content')
				.notNullable()
				.index('idx_resource_content')
				.references('resource_content.uid')
				.onDelete('CASCADE');
			t.json('changes');
        }),

		knex.schema.createTable('resource_content', t => {
            t.increments('uid').primary();
			t.integer('size').notNullable();
			t.string('mime').notNullable();
			t.string('hash', 40).notNullable();
			t.unique(['hash'], 'idx_resource_hash');
			t.binary('content').notNullable();
        }),

    ]);

};

export const down = async function(knex) {
	return Promise.all([
		knex.schema.dropTable('resource')
	]);
};
