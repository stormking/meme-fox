

export const up = async function(knex) {
    return Promise.all([

        // extracted searchable page data
        knex.schema.createTable('history', t => {
            t.increments('uid').primary();
            t.string('url').notNullable();
            t.string('urlInverse').notNullable();
			t.unique(['url'], 'idx_history_url');
            t.datetime('firstVisited').notNullable();
            t.datetime('lastVisited').notNullable();
			t.integer('timesVisited').notNullable().unsigned();
			t.integer('meta')
                .notNullable()
                .index('idx_meta')
                .references('history_meta.uid')
                .onDelete('CASCADE');
        }),

		knex.schema.createTable('history_meta', t => {
            t.increments('uid').primary();
            t.string('hash', 64).notNullable();
			t.unique(['hash'], 'idx_meta_hash');
            t.string('title').notNullable();
            t.text('description').notNullable();
            t.specificType('search', 'tsvector').notNullable();
            t.index(['search'], 'idx_history_search', 'gin');
        })

    ]);

};

export const down = async function(knex) {
	return Promise.all([
		knex.schema.dropTable('history'),
		knex.schema.dropTable('history_meta')
	]);
};
