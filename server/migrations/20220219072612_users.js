
export const up = async function(knex) {
	return Promise.all([
		knex.schema.table('history', t => {
			t.string('user', 64);
			t.index(['user'], 'idx_history_user');
			t.dropUnique(['url'], 'idx_history_url');
			t.unique(['url', 'user'], 'idx_history_url');
			t.index(['firstVisited'], 'idx_history_firstvisited');
			t.index(['lastVisited'], 'idx_history_lastVisited');
		}),
		// knex.schema.table('bookmark', t => {
		// 	t.string('user', 64);
		// 	t.index(['user'], 'idx_bookmark_user');
		// }),
		knex.schema.table('resource_meta', t => {
			t.string('user', 64);
			t.index(['user'], 'idx_resourcemeta_user');
		})
	]);
};

export const down = async function(knex) {
	return Promise.all([
		knex.schema.table('history', t => {
			t.dropIndex(['firstVisited'], 'idx_history_firstvisited');
			t.dropIndex(['lastVisited'], 'idx_history_lastVisited');
			t.dropIndex(['user'], 'idx_history_user')
            t.dropColumn('user');
			t.dropUnique(['url', 'user'], 'idx_history_url');
			t.unique(['url'], 'idx_history_url');
        }),
		// knex.schema.table('bookmark', t => {
		// 	t.dropIndex(['user'], 'idx_bookmark_user');
		// 	t.dropColumn('user', 64);
		// }),
		knex.schema.table('resource_meta', t => {
			t.dropIndex(['user'], 'idx_resourcemeta_user');
			t.dropColumn('user', 64);
		})
	]);
};
