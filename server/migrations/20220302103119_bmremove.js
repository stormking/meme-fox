
export const up = async function(knex) {
	
	await knex.schema.table('history', t => {
		t.boolean('bookmarked').default(false);
	});
	let rows = await knex('bookmark').select('*');
	for (let row of rows) {
		await knex('history').update({ bookmarked: true }).where({ uid: row.history });
	}
	await knex.schema.dropTable('bookmark');
};

export const down = async function(knex) {
	return Promise.all([
		knex.schema.table('history', t => {
			t.dropColumn('bookmarked');
        }),
		knex.schema.createTable('bookmark', t => {
            t.increments('uid').primary();
            t.integer('history')
				.notNullable()
				.unique('idx_history')
				.references('history.uid')
				.onDelete('CASCADE');
			t.string('title');
            t.string('description');
			t.specificType('search', 'tsvector').notNullable();
			t.index(['search'], 'idx_bookmark_search', 'gin');
        })
	]);
};
