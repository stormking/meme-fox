function getInvertedUrl(url) {
	let u = new URL(url);
	let host = u.hostname.split('.').reverse().join('.');
	let inverse = `${host}${u.pathname}`;
	return inverse;
}

export const up = async function(knex) {
	await knex.schema.table('resource_meta', t => {
		t.string('urlInverse', 2048)
	});
	let rows = await knex('resource_meta').select('*');
	for (let row of rows) {
		let urlInverse = getInvertedUrl(row.uri);
		await knex('resource_meta').update({ urlInverse }).where({ uid: row.uid });
	}
};

export const down = async function(knex) {
	return Promise.all([
		knex.schema.table('resource_meta', t => {
            t.dropColumn('urlInverse');
        })
	]);
};
