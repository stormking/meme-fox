

export const up = async function(knex) {
    return Promise.all([

        // extracted searchable page data
        knex.schema.createTable('bookmark', t => {
            t.increments('uid').primary();
            t.integer('history')
				.notNullable()
				.unique('idx_history')
				.references('history.uid')
				.onDelete('CASCADE');
            t.string('memo');
			t.json('tags');
			t.specificType('search', 'tsvector').notNullable();
			t.index(['search'], 'idx_bookmark_search', 'gin');
        })

    ]);

};

export const down = async function(knex) {
	return Promise.all([
		knex.schema.dropTable('bookmark')
	]);
};
