

export const up = async function(knex) {
    return Promise.all([

        // extracted searchable page data
        knex.schema.table('bookmark', t => {
            t.string('title');
            t.renameColumn('memo', 'description');
			t.dropColumn('tags');
        })

    ]);

};

export const down = async function(knex) {
	return Promise.all([
		knex.schema.table('bookmark', t => {
			t.dropColumn('title');
			t.renameColumn('description', 'memo');
			t.json('tags');
		})
	]);
};
