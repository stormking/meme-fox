# Meme-Fox Server development

## Project setup

Node v16+
```
npm i
```

### Start development
```
docker-compose -f docker-compose.dev.yaml up
npm run watch
npm run start
```


### Run tests
```
docker-compose -f docker-compose.dev.yaml up
npm run test
```
Deletes and rebuilds the database.
