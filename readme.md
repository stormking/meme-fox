# The Meme Fox

A self-hosted browser extension for searchable history, bookmark, page-snapshots, and sharing functions.

## Current Functions

- History is stored and searchable

## Future Developments

- can save, search and annotate bookmarks
- multi-user option via OIDC
- save snapshots of pages and display them later
- edit and annotate snapshots before saving them
- federalize to other servers via activity pub

- ffx bookmark import / sync
- server check which pages still exist and show in list
- offline sync to different devices

## Installation

Deployment with docker is recommended.

- Pre-requisite: you need to have have installed [docker](https://docs.docker.com/engine/install/debian/#install-using-the-convenience-script) and [docker-compose](https://docs.docker.com/compose/install/)
- get the docker-compose [file](https://gitlab.com/stormking/meme-fox/-/blob/master/docker-compose.yaml)
- edit the config (see below)
- `docker-compose up -d` starts the app including the database in the background
- install the browser-extension from ...


### Configuration:

All config settings are specified via ENV-vars.

- with docker: set the variables in your docker service / docker-compose.yml / docker-compose.arm.yaml
- without docker: copy .env.sample to .env and edit that file

ENV vars set in docker-compose.yaml override vars in the .env file.

Config options:

DATABASE_URL - full uri to postgres database including name/pw/db
(example: postgres://test:test@localhost:5432/test)

BASE_URL - the external url how the app is reachable. required to build the feeds
(example: http://localhost:8080)

APP_PORT - port for listening to http connections
(example: 3000)

DEBUG - for debug output, see npm debug package for details
(example: ap*)
